#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  5 22:16:15 2019

@author: me
"""

import sys

file_in = sys.argv[1]
file_out = sys.argv[2]

with open(file_in) as file_in_fd, open(file_out, 'w') as file_out_fd:
    for line in file_in_fd:
        split = line.split()
        values = [float(elem) for elem in split[1:]]
        max_values = max(values)
        str_values_out = ['1.0' if elem==max_values else "0.0" for elem in values]
        file_out_fd.write(split[0] + ' ' + ' '.join(str_values_out) + '\n')
