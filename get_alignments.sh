. path.sh

ali=tri5b_ali_clean_360

for i in exp/$ali/ali.*.gz;
do
  ali-to-phones --ctm-output exp/$ali/final.mdl ark:"gunzip -c $i|" -> ${i%.gz}.ctm;
done;

cat exp/$ali/*.ctm > exp/$ali/merged_alignment.txt
