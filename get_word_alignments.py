ali='tri5b_ali_clean_360'

phn_alignments_file='exp/'+ali+'/merged_alignment.txt'
phn_file='exp/'+ali+'/phones.txt'
transcripts='data/train_clean_360/text'

def load_transcripts():
    transcriptions = {}
    with open(transcripts) as transcripts_file_in:
        for line in transcripts_file_in:
            split = line.split()
            transcriptions[split[0]] = split[1:]
    return transcriptions

def load_phn_file():
    phone_ids = []
    with open(phn_file) as phn_file_in:
        for line in phn_file_in:
            split=line.split()
            phone_id = split[1]
            phone = split[0]
            phone_ids.append(phone)
    return phone_ids

#print('Loading phones...')
phone_ids = load_phn_file()
#print('Loading transcriptions...')
transcriptions_dict = load_transcripts()
#print(phone_ids)

eps = 0.000001

with open(phn_alignments_file) as alignment_file:
    last_id = ''
    word_pos = 0
    for line in alignment_file:
        #format
        #ZeresenayAlemseged_2007G-0029902-0031120-1 1 11.930 0.060 121
          
        split=line.split()
        myid = split[0]
        start_pos = split[2]
        len_pos = split[3]
        phone = phone_ids[int(split[4])]
        split_phone = phone.split('_')[0]        

        #new id
        if last_id != myid:
            word_pos = 0

        #begin marker
        if phone.endswith('B'):
            start_seq_pos = start_pos
            phn_seq = [split_phone]
            length = float(len_pos) + eps
        #intermediate marker
        elif phone.endswith('I'):
            phn_seq += [split_phone]
            length += float(len_pos) + eps
        #end marker
        elif phone.endswith('E'):
            phn_seq += [split_phone]
            length += float(len_pos) + eps
            len_str = "%0.3f" % length 
            word = transcriptions_dict[myid][word_pos]
            print(myid, 1, start_seq_pos, len_str, word, ' '.join(phn_seq))
            word_pos += 1
        #single marker
        elif phone.endswith('S'):
            #if phone != 'NSN':
            word = transcriptions_dict[myid][word_pos] 
            print(myid, 1, start_pos, len_pos, word, split_phone)
            word_pos += 1
        else:
            if split_phone != 'SIL':
                print('problem', phone)
        last_id = myid
