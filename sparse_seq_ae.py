#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 19:28:22 2019

@author: Benjamin Milde
"""

license = '''

Copyright 2019 Benjamin Milde (Language Technology, Universität Hamburg, Germany)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.'''

import tensorflow as tf
import tensorflow.contrib.slim as slim

import numpy as np
import pylab as plt

from sklearn import metrics
from sklearn.metrics import accuracy_score

import six
import time
import math
import sys
import os
import kaldi_io
import myutils
import itertools
import pickle
from itertools import groupby

from dtw import dtw

from scipy.signal import convolve2d
from numpy.linalg import norm

import memory
import random
from memory import memory_augment

import sklearn

from sklearn import mixture
from sklearn.utils import shuffle
from sklearn.cluster import KMeans, DBSCAN
from sklearn.mixture import BayesianGaussianMixture

from tensorflow.python.training import input as input_lib
from tensorflow.contrib.factorization.python.ops import kmeans as kmeans_lib

from matplotlib.backends.backend_pdf import PdfPages

try:
    import ABXpy.distances.metrics.dtw as dtw
    import ABXpy.distances.metrics.cosine as cosine
    import ABXpy.distances.metrics.kullback_leibler as kullback_leibler
except:
    print("Warning, ABX py not found. See https://github.com/bootphon/ABXpy. You can use sparsespeech without it, but you wont be able to use evaluation.")
    no_ABXpy=True

try:
    import hdbscan
except:
    print("Hdbscan not found. You can still use kmeans as cluster init algorithm.")

if sys.version_info[0] == 3:
    xrange = range

flags = tf.app.flags
FLAGS = tf.app.flags.FLAGS

flags.DEFINE_string("modelname","e2e", "Additional string identifier to use in output dir (optional)")

#flags.DEFINE_string("filelist", "feats/tedlium/train/unnormalized.feats.nodither.ark", "Kaldi scp file if using kaldi feats, or for end-to-end learning a simple filelist, one wav file per line, optionally also an id (id wavfile per line).")
#flags.DEFINE_string("spk2utt", "feats/tedlium/train/spk2utt_cleaned", "Optional, but required for per speaker negative sampling. ")
#flags.DEFINE_string("ali_ctm", "feats/tedlium/train/alignments.txt" , "Alignments for all ids in ctm format")
#flags.DEFINE_string("ali_word_ctm", "feats/tedlium/train/word_phn_alignments.txt" , "Alignments for all ids in ctm format")
#flags.DEFINE_string("phones", "feats/tedlium/train/phones.txt" , "Kaldi phones file for alignment file")

#flags.DEFINE_string("filelist", "feats/librispeech_360/mfcc_hires_360.ark", "Hires MFCC")
flags.DEFINE_string("filelist_context_features", "", "Hires MFCC")
#flags.DEFINE_string("filelist_context_features", "feats/librispeech_360/librispeech_360_unspeech32_256.ark", "Hires MFCC")
#flags.DEFINE_string("filelist", "feats/librispeech_360/train_clean_360.ark", "Kaldi scp file if using kaldi feats, or for end-to-end learning a simple filelist, one wav file per line, optionally also an id (id wavfile per line).")
flags.DEFINE_string("filelist", "feats/librispeech_360/plp_360_feats.ark", 
                    "Kaldi scp file if using kaldi feats, or for end-to-end learning a simple filelist, one wav file per line, optionally also an id (id wavfile per line).")
flags.DEFINE_string("spk2utt", "feats/librispeech_360/spk2utt", "Optional, but required for per speaker negative sampling. ")
flags.DEFINE_string("ali_ctm", "feats/librispeech_360/alignments_360.txt" , "Alignments for all ids in ctm format")
flags.DEFINE_string("ali_word_ctm", "feats/librispeech_360/word_alignments_360.txt" , "Alignments for all ids in ctm format")
flags.DEFINE_string("phones", "feats/librispeech_360/phones.txt" , "Kaldi phones file for alignment file")

flags.DEFINE_integer("fc_size", 32 , "Fully connected size at the end of the network. Note that for memory augmentation this needs to be equal to embedding_dim.") 
flags.DEFINE_integer("embedding_dim", 32, "For the memory augmentation: dimension of the embeddings in the memory bank")
flags.DEFINE_integer("num_mem_entries", 32, "For the memory augmentation: number of entries in the memory bank (n)")

flags.DEFINE_boolean("use_fused_LSTM", True, "Whether to use the fused or standard LSTM implementation")
flags.DEFINE_integer("rnn_hidden_cells", 256, "Size of hidden cells for recurrent neural networks, e.g. if LSTMs or GRUs are used in the embedding transform.")
flags.DEFINE_integer("rnn_layers", 2 , "Number of stacked RNN layers.")
flags.DEFINE_boolean("rnn_add_fw_bw", False ,  "If true, then RNN backward and forward passes will be added. If false, they will be concatenated")

flags.DEFINE_float("dropout_keep_prob", 1.0, "Dropout keep probability")
flags.DEFINE_float("l2_reg", 0.0, "L2 regularization") # was 0.0005

flags.DEFINE_string("loss","rms", "What loss function to use")
flags.DEFINE_string("device","/gpu:1", "Computation device, e.g. /gpu:1 for 1st GPU.")

flags.DEFINE_string("encoder_device","/gpu:1", "Computation device for the encoder.")
flags.DEFINE_string("bridge_device","/gpu:1", "Computation device for the bridge between encoder and decoder (including memory component if activated)")
flags.DEFINE_string("decoder_device","/gpu:1", "Computation device for the decoder.")
flags.DEFINE_string("loss_device","/gpu:1", "Computation device for the final loss computation.")

flags.DEFINE_integer("rnn_context_vector_layer", 0, "Take this RNN layer to create the context vector. Defaults to 0 (first RNN layer).")

flags.DEFINE_integer("batch_size", 32, "Batch Size (default: 32)")
flags.DEFINE_float("learn_rate", 1e-4, "Learn rate for the main optimizer (ADAM)")
flags.DEFINE_float("learn_rate_rms_prop", 5e-3, "Learn rate for the auxillary RMSProp optimizer (for the memory component)")
flags.DEFINE_float("adam_epsilon", 1e-8, "Adam optimizer epsilon. Use something like 1e-4 for float16.")
flags.DEFINE_float("gradient_clipping", 50.0, "Clip the gradient at larger +/- this value.")

flags.DEFINE_string("activation_function", "lrelu", "The activation function that is used after dense layers.")

flags.DEFINE_integer("steps_per_checkpoint", 100,
                                "How many training steps to do per checkpoint.")
flags.DEFINE_integer("steps_per_summary", 100,
                                "How many training steps to do per summary.")

flags.DEFINE_integer("checkpoints_per_save", 5,
                                "How many checkpoints until saving the model.")

flags.DEFINE_boolean("log_tensorboard", True, "Log training process if this is set to True.")

#flags.DEFINE_string("train_dir", "/home/me/sparsespeech/resumed/1564588282sparsespeech_e2e_alictm_alignments_360.txt_bs32_lstmdim256_lstmlayers2_embdim42_num_mem_entries42_cluster__sparsity_reg0.0_diversity_reg_diversity_reg_kl0.0_dwelling_reg0.0_gumbel_softmax_ccconcat_seq_dropoutkeep0.33_max_epochs20/","Training dir to resume training from. If empty, a new one will be created.")

#flags.DEFINE_string("train_dir", "","Training dir to resume training from. If empty, a new one will be created.")
flags.DEFINE_string("train_dir", "/srv/data/milde/unspeech_models/sparse_ae/runs/runs/1553820097sparsespeech_plp_alictm__bs32_lstmdim256_lstmlayers2_embdim32_num_mem_entries80_sparsity_reg200.0_diversity_reg0.0_dwelling_reg0.0_ccconcat_seq_dropoutkeep0.33_max_epochs30/","Training dir to resume training from. If empty, a new one will be created.")

flags.DEFINE_integer("max_to_keep_models", 20, "Maximum number of models the trainer should keep")
#flags.DEFINE_string("train_dir", "/home/me/sparsespeech/runs/1553826612sparsespeech_fbank_alictm_alignments_360.txt_bs32_lstmdim256_lstmlayers2_embdim32_num_mem_entries80_sparsity_reg200.0_diversity_reg0.0_dwelling_reg0.0_ccconcat_seq_dropoutkeep0.2_max_epochs30/", "")
#flags.DEFINE_string("train_dir", "/srv/data/milde/unspeech_models/sparse_ae/runs/1552944952sparsespeech_alictm_alignments.txt_bs64_lstmdim256_embdim10_num_mem_entries80_ccconcat", "")
#flags.DEFINE_string("train_dir", "/srv/data/milde/unspeech_models/sparse_ae/runs/1552827200sparsespeech_alictm_alignments.txt_bs64_lstmdim256_embdim128_num_mem_entries80_ccconcat.copy", "")
#flags.DEFINE_string("train_dir","/srv/data/milde/unspeech_models/sparse_ae/runs/1552755507sparsespeech_alictm_alignments.txt_bs64_lstmdim256_embdim256_num_mem_entries80_ccconcat/", "")
#flags.DEFINE_string("train_dir", "/srv/data/milde/unspeech_models/sparse_ae/runs/1552684418sparsespeech_alictm_alignments.txt_bs64_embdim100_num_mem_entries80", "")
#flags.DEFINE_string("train_dir", "/srv/data/milde/unspeech_models/sparse_ae/runs/1552517629sparsespeech_alictm_alignments.txt_bs64_embdim100_num_mem_entries42/", "Training dir to resume training from. If empty, a new one will be created.")
#flags.DEFINE_string("train_dir", "/srv/data/milde/unspeech_models/sparse_ae/runs/1552580415sparsespeech_alictm_alignments.txt_bs64_embdim100_num_mem_entries80/", "")
#flags.DEFINE_string("train_dir", "/srv/data/milde/unspeech_models/sparse_ae/runs/1552869874sparsespeech_alictm_alignments.txt_bs32_lstmdim256_embdim128_num_mem_entries80_ccconcat/", "")
#flags.DEFINE_string("train_dir", "/srv/data/milde/unspeech_models/sparse_ae/runs/1553047142sparsespeech_alictm_alignments.txt_bs64_lstmdim256_embdim16_num_mem_entries80_ccconcat/", "")
#flags.DEFINE_string("train_dir","/srv/data/milde/unspeech_models/sparse_ae/runs/1553281748sparsespeech_alictm_alignments.txt_bs64_lstmdim256_embdim32_num_mem_entries80_ccconcat/", "")
#flags.DEFINE_string("train_dir", "/srv/data/milde/unspeech_models/sparse_ae/runs/1553304529sparsespeech_alictm_alignments.txt_bs64_lstmdim256_embdim32_num_mem_entries80_ccconcatseq_dropout0.33/", "Training dir to resume training from. If empty, a new one will be created.")
flags.DEFINE_integer("train_epochs", 20, "Number of training epochs.")

flags.DEFINE_boolean("evaluate", False, "Only do evaluations on the model in train_dir or the list of models in evaluate_models")
flags.DEFINE_boolean("evaluate_after_train", True,  "Evaluate directly after training.")
flags.DEFINE_string("evaluate_models", "" , "Separated list of models to evaluate (separated by :) or empty string")
flags.DEFINE_boolean("baseline_feat_eval", False, "Evaluate the baseline features directly")
flags.DEFINE_boolean("only_eval_pretrain", False, "Only eval pretrain model")
flags.DEFINE_boolean("bridge_without_concat", False, "Output bridge_without_concat (embeddings of the symbols) when genereating zero speech challenge feats")
flags.DEFINE_boolean("gen_bypass", False, "Output the input features")
flags.DEFINE_string("gen_zerospeech_dir", "", "If evaluate= True and a path is set, write to zerospeech format")
flags.DEFINE_string("gen_kaldi_file", "", "If evaluate= True and a path is set, write to Kaldi text file")
flags.DEFINE_boolean("dtw_normalized", False, "Whether paths should be normalized in DTW computations")
flags.DEFINE_boolean("DPGMM_cluster_baseline", False, "Run the DPGMM baseline")
flags.DEFINE_string("DPGMM_baseline_model", "" , "Use this baseline model directly on the features for evaluation.") #"dpgmm_ne665069_maxiter100.pickle"

flags.DEFINE_boolean("curriculum_learning", True, "Sort batches by size.")

flags.DEFINE_string("context_combine", "concat", "One of 'add', 'concat', 'post_add', 'post_concat' or 'none'.")
flags.DEFINE_integer("context_projection", -1, "Size of the context features. Using -1 disables context projection completely.")
flags.DEFINE_integer("max_seq_len", 2000, "Maximum sequence length for utterances, discard sequences longer than that.")

flags.DEFINE_integer("memory_augment_epoch", 0, "Do memory augmentation after this many steps.") #20000
flags.DEFINE_integer("memory_init_samples", 10, "Use this many samples (batches of sequences) for kmeans initialization.")
#flags.DEFINE_integer("memory_init_samples", 500, "Use this many samples (batches of sequences) for kmeans initialization.")
flags.DEFINE_integer("memory_subnetwork_trainingsteps", 6000, "Do this many training steps with the memory subnetwork.")
flags.DEFINE_boolean("memory_subnetwork_use_minibatch", False, "Use minibatch training for the subnetwork initialization.")

flags.DEFINE_string("memory_softmax_type", "gumbel_softmax", "The type of softmax in the memory cell. Either 'softmax', 'sparsemax', 'gumbel_softmax'")

flags.DEFINE_float("bottleneck_dropout_keep_prob", .33, "Bottleneck probability for dropping elements randomly in a sequence.")
flags.DEFINE_float("sparsity_regularizer_scale", 0.0, "Scale parameter of the additional sparsity inducing loss.") #was 0.001
flags.DEFINE_float("grow_sparsity_regularizer_scale", 0.0, "If not 0.0, then the sparsity constraints grows by this much every epoch (and starts at zero).") #was 0.001
flags.DEFINE_float("diversity_regularizer_scale", 100.0, "Scale parameter of the additional diversity inducing loss.") # was 500
flags.DEFINE_string("diversity_loss_type","kl","Either simple (reduce maximum of the average across the utterance, as in the sparsepseech paper) or using kl divergence on a uniform distribution.")
flags.DEFINE_float("dwelling_regularizer_scale", 0, "Scale parameter of the additional diversity inducing loss.") # was 5000

flags.DEFINE_float("gumbel_temperature_start", 2.0, "Starting temperature of the gumbel softmax.") # was 5000
flags.DEFINE_float("gumbel_temperature_stop", 0.001, "Stop decreasing temperature for gumbel softmax if this temperature is reached.")
flags.DEFINE_float("gumbel_temperature_scale", 0.999, "Scale parameter of the gumbel softmax temperature (temperature is multiplied with this value every epoch).") # was 5000

flags.DEFINE_float("gumbel_generate_temperature", 0.001, "The gumbel temperature to use use when evaluating or generating features.")

flags.DEFINE_integer("n_jobs_cluster", 14, "How many CPU cores to use for cluster initialization.")

flags.DEFINE_boolean("smooth_feats", False, "Whether to smooth the input representation slightly")
flags.DEFINE_boolean("pretrain_kmeans", True, "Initialize the values of the memory bank to kmeans centers.")
flags.DEFINE_boolean("use_tensorflow_kmeans", True , "Whether to use the Tensorflow kmeans implementation or the sklearn one.")

flags.DEFINE_boolean("fix_encoder_weights", False, "Fixing the encoder weights.")

flags.DEFINE_boolean("pylab_show_feats", True, "Show features and memory bank (with pylab) while training at every summary step.")
flags.DEFINE_boolean("pylab_savefigs_pdf", True, "Show features and memory bank (with pylab) while training at every summary step.")

flags.DEFINE_boolean("scan_sparsity_values", False, "Scan a range of sparsity values and evaluate.")
flags.DEFINE_integer("ABX_eval_samples", 500, "ABX eval samples")
flags.DEFINE_integer("demo_utterance_num", 420, "A fixed utterance from the sorted list, to show in pylab and saved to the pdf at each checkpoint step.")

flags.DEFINE_boolean("debug", False, "Limits the filelist size and is more debug.")
flags.DEFINE_integer("debug_limit", 4000, "Limits the filelist size")

flags.DEFINE_string("cluster_algo", "sklearn_kmeans" , "tensorflow_kmeans, sklearn_kmeans, hdbscan or none")

flags.DEFINE_string("utt_len_file",  "", "")

flags.DEFINE_string("dtype", "float32", "The dtype to use to construct the model (defaults to float32)")
#flags.DEFINE_string("utt_len_file",  "feats/tedlium/train/len.txt", "")


# Format: dict, utterance_id -> numpy array (fbank/mfcc/plp ... features)
training_data = {}
#TODO load this correctly. Format: dict, spk -> list utt ids
spk2utt_data = {}

# Format: dict, utterance_id -> list of (start,end,id) element tuples
alignment_data = {}

# Format: dict, utterance_id -> numpy array (fbank/mfcc/plp ... features)
context_training_data = {}

phones_data = {}
phones_reduced_trans_data = {}
phones_reduced_trans_data_phn = {}
phones_reduced = []

results_table_header = sorted(["ARI","NMI"])
results_table_header_dict = dict([(x[1],x[0]) for x in enumerate(results_table_header)])

number_format = "%.4f"

utt_len_sampler = None

def get_FLAGS_params_as_str():
    params_str = ''
    for attr, value in sorted(FLAGS.flag_values_dict().items()):
        params_str += "{}={}\n".format(attr, value)
    return params_str

def get_model_flags_param_short():
    """ get model command line params (in FLAGS) as a short string, e.g. to use it in an output filepath """
    return "sparsespeech_" + (FLAGS.modelname + "_" if FLAGS.modelname != '' else '') + "alictm_" + FLAGS.ali_ctm.split("/")[-1] + "_bs" + \
    str(FLAGS.batch_size) + "_lstmdim"+ str(FLAGS.rnn_hidden_cells) + "_lstmlayers"+ str(FLAGS.rnn_layers) + "_embdim" + \
    str(FLAGS.embedding_dim) + "_num_mems" + str(FLAGS.num_mem_entries) + '_cluster_' + str(FLAGS.DPGMM_baseline_model) + \
    "_spars_reg" + str(FLAGS.sparsity_regularizer_scale) + "_div_reg" + str(FLAGS.diversity_regularizer_scale) + "_" + FLAGS.diversity_loss_type + \
    "_dw_reg" + str(FLAGS.dwelling_regularizer_scale) + "_" + FLAGS.memory_softmax_type + \
    ("_gg" + str(FLAGS.gumbel_generate_temperature) if FLAGS.memory_softmax_type == 'gumbel_softmax' else '') + \
    "_cc" + FLAGS.context_combine + ("_cproj" + str(FLAGS.context_projection) if FLAGS.context_projection != -1 else "") + \
    "_dkeep" + str(FLAGS.bottleneck_dropout_keep_prob) + "_epochs" + str(FLAGS.train_epochs) + "_" + FLAGS.activation_function + "_" \
    + FLAGS.dtype if FLAGS.dtype != "float32" else ""

def lrelu(x, leak=0.2, name="lrelu"):
    """ Leaky Rectified Linear activation, as first introduced in acoustic modelling in (Maas 2013)
       "Rectifier Nonlinearities Improve Neural Network Acoustic Models" , 2013 , Maas et al. ,
       https://ai.stanford.edu/~amaas/papers/relu_hybrid_icml2013_final.pdf
       Leakly relu help to circumvent the dieing ReLU problem
    :param x: input tensor
    :param leak: leak parameter
    :param name:
    :return: leaky relu activation of the input tensor
    """
    return tf.maximum(x, leak*x)

# from https://github.com/google-research/bert/blob/master/modeling.py
def gelu(x):
    """Gaussian Error Linear Unit.
    This is a smoother version of the RELU.
    Original paper: https://arxiv.org/abs/1606.08415
    Args:
    x: float Tensor to perform activation.
    Returns:
    `x` with the GELU activation applied.
    """
    cdf = 0.5 * (1.0 + tf.tanh(
      (np.sqrt(2 / np.pi) * (x + 0.044715 * tf.pow(x, 3)))))
    return x * cdf

# adapted from https://github.com/google-research/bert/blob/master/modeling.py
def get_activation(activation_string):
    """Maps a string to a Python function, e.g., "relu" => `tf.nn.relu`.
    Args:
    activation_string: String name of the activation function.
    Returns:
    A Python function corresponding to the activation function. If
    `activation_string` is None, empty, or "linear", this will return None.
    If `activation_string` is not a string, it will return `activation_string`.
    Raises:
    ValueError: The `activation_string` does not correspond to a known
      activation.
    """

    # We assume that anything that"s not a string is already an activation
    # function, so we just return it.
    if not isinstance(activation_string, six.string_types):
        return activation_string

    if not activation_string:
        return None

    act = activation_string.lower()
    if act == "linear":
        return None
    elif act == "relu":
        return tf.nn.relu
    elif act == "gelu":
        return gelu
    elif act == "lrelu":
        return lrelu
    elif act == "tanh":
        return tf.tanh
    else:
        raise ValueError("Unsupported activation: %s" % act)

def get_dtype(dtype_string):
    """Maps a string to a Tensorflow dtype"""
    if not isinstance(dtype_string, six.string_types):
        return dtype_string

    if not dtype_string:
        return None

    dtype_string = dtype_string.lower()
    if dtype_string == "float16":
        return tf.float16
    elif dtype_string == "float32":
        return tf.float32
    elif dtype_string == "bfloat16":
        return tf.bfloat32
    else:
        raise ValueError("Unsupported dtype: %s" % act)

def kl(p, q):
    P = tf.distributions.Categorical(probs=p)
    Q = tf.distributions.Categorical(probs=q)
    return tf.distributions.kl_divergence(P, Q, allow_nan_stats=False)

# get a padded batch with random sequences from the corpus
def get_batch_ordered(start_index, batch_size, curriculum_utt_id_list, idlist_size, context_feature=False, return_ids=False):
    #idlist_size = len(curriculum_utt_id_list)
    data_batch = []
    padded_data_batch = []
    len_batch = []
    context_features = []
    ids = []
    
    for i in range(start_index,start_index+batch_size):
        if i >= idlist_size:
            break
        data = training_data[curriculum_utt_id_list[i]]

        data_batch.append(data)
        len_batch.append(data.shape[0])
        
        if context_feature:
            context_features.append(context_training_data[curriculum_utt_id_list[i]])
            
        if return_ids:
            ids.append(curriculum_utt_id_list[i])
    
    max_seq_len = max(len_batch)
    
    padded_data_batch = list(data_batch)
 
    for i,seq in enumerate(data_batch):
        new_seq = np.zeros((max_seq_len,seq.shape[1]))
        new_seq[0:seq.shape[0]] = seq
        padded_data_batch[i] = new_seq
    
    if return_ids:
        return padded_data_batch,len_batch, context_features, ids
    
    return padded_data_batch,len_batch, context_features

# get a padded batch with random sequences from the corpus
def get_batch(batch_size, utt_id_list, idlist_size, context_feature=False):
    #idlist_size = len(utt_id_list)
    data_batch = []
    padded_data_batch = []
    len_batch = []
    context_features = []
    
    for i in range(batch_size):
        random_id_num = int(math.floor(np.random.random_sample() * float(idlist_size)))
        data = training_data[utt_id_list[random_id_num]]
        data_batch.append(data)
        len_batch.append(data.shape[0])
        
        if context_feature:
            context_features.append(context_training_data[utt_id_list[random_id_num]])
    
    max_seq_len = max(len_batch)
    
    padded_data_batch = list(data_batch)
 
    for i,seq in enumerate(data_batch):
        new_seq = np.zeros((max_seq_len,seq.shape[1]))
        new_seq[0:seq.shape[0]] = seq
        padded_data_batch[i] = new_seq
    
    return padded_data_batch, len_batch, context_features


# get a padded batch with the requested utterances in utt_id_list
def get_batch_specific(utt_id_list, context_feature=False):
    
    data_batch = []
    padded_data_batch = []
    len_batch = []
    context_features = []
    
    for utt_id in utt_id_list:
        data = training_data[utt_id]
        data_batch.append(data)
        len_batch.append(data.shape[0])
        
        if context_feature:
            context_features.append(context_training_data[utt_id])
    
    max_seq_len = max(len_batch)
    
    padded_data_batch = list(data_batch)
 
    for i,seq in enumerate(data_batch):
        new_seq = np.zeros((max_seq_len,seq.shape[1]))
        new_seq[0:seq.shape[0]] = seq
        padded_data_batch[i] = new_seq
    
    return padded_data_batch, len_batch, context_features


# The memory subnetwork trains the memory network to 
class MemorySubnetwork(object):
    def __init__(self, batch_size=32):
        #self.key_memory = tf.Variable(tf.random_normal([FLAGS.num_mem_entries, FLAGS.embedding_dim],  stddev=0.1), name="key_memory", trainable=True)
        self.value_memory = tf.Variable(tf.random_normal([FLAGS.num_mem_entries, FLAGS.embedding_dim],  stddev=0.1, dtype=get_dtype(FLAGS.dtype)),
                                        name="value_memory", trainable=True, dtype=get_dtype(FLAGS.dtype))
        
        self.key_memory_W = tf.Variable(tf.random_normal([FLAGS.embedding_dim, FLAGS.num_mem_entries],  stddev=0.1, dtype=get_dtype(FLAGS.dtype)),
                                        name="key_memory_W", trainable=True, dtype=get_dtype(FLAGS.dtype))
        self.key_memory_b = tf.Variable(tf.random_normal([FLAGS.num_mem_entries],  stddev=0.1, dtype=get_dtype(FLAGS.dtype)),
                                        name="key_memory_b", trainable=True, dtype=get_dtype(FLAGS.dtype))
        
        self.enc_out_flattened_in = tf.placeholder(get_dtype(FLAGS.dtype), [None, FLAGS.embedding_dim], name="enc_out_flattened_in")
        self.labels_in = tf.placeholder(tf.int32, [None], name="labels_in")
        
        context_mem_out, self.query_out, self.mul_query_key_memory_reduced, sparsity_loss = memory_augment(self.enc_out_flattened_in, self.key_memory_W, self.key_memory_b, self.value_memory, print_tensor_dims=True, 
                                                                                                          softmax_type='softmax' if not FLAGS.memory_softmax_type == 'gumbel_softmax' else 'softmax',
                                                                                                          sparsity_regularizer_norm='l2', pre_relu=False, euclidean_normalization=False,
                                                                                                          with_tanh=(True if FLAGS.memory_softmax_type == 'gumbel_softmax' else False),
                                                                                                          dtype=get_dtype(FLAGS.dtype))
        
        self.cost = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.mul_query_key_memory_reduced, labels=self.labels_in))
        
        
        self.global_step = tf.Variable(0, name="global_step", trainable=False)
        self.optimizer = tf.train.RMSPropOptimizer(FLAGS.learn_rate_rms_prop)
        
        self.train_vars = [self.key_memory_W, self.key_memory_b]
     
    def create_train_op(self):    
        print("Creating train_op for memory subnetwork:", self.train_vars)      
        self.train_op = slim.learning.create_train_op(self.cost, self.optimizer, global_step=self.global_step,
                                                      clip_gradient_norm=FLAGS.gradient_clipping, variables_to_train=self.train_vars)


# TODO: simple quantization baseline
def train_VQ(n=20, modelname="VQ_baseline"):
    
    enc_out_stacked = np.vstack(training_data)
    
    kmeans = KMeans(n_clusters=FLAGS.num_mem_entries, init='k-means++', n_init = FLAGS.n_jobs_cluster,
                                                                             n_jobs = FLAGS.n_jobs_cluster).fit(enc_out_stacked)
    cluster_centers = np.asarray(kmeans.cluster_centers_)                       
    predictions = np.asarray(kmeans.labels_)
    
    filename = modelname + "_" + str(n)


class baselineDPGMM(object):
    # demo utterance 4535-279852-0038
    def __init__(self, max_iter=200, n_components=80):
        self.max_iter = max_iter
        self.n_components = n_components
        for key in training_data:    
            self.feat_size = training_data[key].shape[-1]
            break
        #This is a DPGMM in sklearn:
        self.model = sklearn.mixture.BayesianGaussianMixture(n_components=n_components, covariance_type='full',
                                                            tol=0.001, reg_covar=1e-06, max_iter=self.max_iter,
                                                            n_init=1, init_params='kmeans',
                                                            weight_concentration_prior_type='dirichlet_process',
                                                            weight_concentration_prior=None, mean_precision_prior=None,
                                                            mean_prior=None, degrees_of_freedom_prior=None,
                                                            covariance_prior=None, random_state=42,
                                                            warm_start=False, verbose=1000, verbose_interval=1)
        self.trained = False
        self.number_elements = 0
    def train(self, utt_list):
        X = np.vstack([training_data[key] for key in utt_list])
        self.number_elements = X.shape[0]
        print('Starting fitting on:', self.number_elements, 'elements.')
        self.model.fit(X)
        self.trained = True
    def predict(self, utterance):
        return self.model.predict(utterance)
    def predict_proba(self, utterance):
        return self.model.predict_proba(utterance)
    def save(self, modelname):
        if self.trained:
            filename = modelname + ("_dpgmm_ne%d_maxiter%d_maxcomponents%d.pickle" % (self.number_elements,self.max_iter, self.n_components))
            with open(filename,"wb") as pickle_out:
                pickle.dump(self.model, pickle_out)
            return filename
        else:
            print('Cant save, not trained yet.')
    def load(self, modelpath):
        self.trained = True
        with open(modelpath, 'rb') as input_f:
            self.model = pickle.load(input_f)      
        
class SparseSpeech(object):
    """
    Sparsifying speech signals
    """
    def __init__(self, batch_size=32, context_feat_size=256, with_context_vector=False, is_training=True,
                 create_new_train_dir=True, do_memory_augment=False, create_training_graphs=True):
        
        for key in training_data:    
            feat_size = training_data[key].shape[-1]
            break
        
        self.batch_size = batch_size
        self.feat_size = feat_size
        self.context_feat_size = context_feat_size
        self.with_context_vector = with_context_vector
        
        self.input_sequence = tf.placeholder(get_dtype(FLAGS.dtype), [None, None, feat_size], name="input_sequence")
        self.output_sequence = tf.placeholder(get_dtype(FLAGS.dtype), [None, None, feat_size], name="output_sequence")
        
        self.sparsity_weight = tf.placeholder(get_dtype(FLAGS.dtype), [], name="sparsity_weight")
        self.gumbel_temperature = tf.placeholder(get_dtype(FLAGS.dtype), [], name="gumbel_temperature")
        self.gumbel_noise_weight = tf.placeholder(get_dtype(FLAGS.dtype), [], name="gumbel_noise_weight")
        
        if do_memory_augment:
            self.key_memory_W = tf.Variable(tf.random_normal([FLAGS.embedding_dim, FLAGS.num_mem_entries], stddev=0.1, dtype=get_dtype(FLAGS.dtype)),
                                            name="key_memory_W", trainable=True, dtype=get_dtype(FLAGS.dtype))
            self.key_memory_b = tf.Variable(tf.random_normal([FLAGS.num_mem_entries],  stddev=0.1, dtype=get_dtype(FLAGS.dtype)), name="key_memory_b",
                                            trainable=True, dtype=get_dtype(FLAGS.dtype))
            self.value_memory = tf.Variable(tf.random_normal([FLAGS.num_mem_entries, FLAGS.embedding_dim], dtype=get_dtype(FLAGS.dtype), stddev=0.1),
                                            name="value_memory", trainable=True, dtype=get_dtype(FLAGS.dtype))
            self.do_memory_augment = True
        
        if with_context_vector:
            # one context vector per sequence
            self.context_vector = tf.placeholder(get_dtype(FLAGS.dtype), [None, context_feat_size], name="context_vector")
            
        # the actual lengths of the sequences as input int tensor,
        # so that the gradient of LSTM can be stopped at the correct times
        self.input_sequence_length = tf.placeholder(tf.int32, [None], name="input_sequence_length")

        # changed tf.constant_initializer(0.001 -> 0.01 due to float16
        with slim.arg_scope([slim.fully_connected],  weights_initializer=tf.variance_scaling_initializer(dtype=get_dtype(FLAGS.dtype)),
                                    weights_regularizer=slim.l2_regularizer(FLAGS.l2_reg) if FLAGS.l2_reg != 0.0 else None,
                                    activation_fn=get_activation(FLAGS.activation_function),
                                    biases_initializer=tf.constant_initializer(0.01, dtype=get_dtype(FLAGS.dtype))):
            
            print('Using dynamic biLSTM Encoders/Decoders')
            
            enc_trainable = False if (FLAGS.fix_encoder_weights and do_memory_augment) else True
            
            print('Encoder trainable = ', enc_trainable)

            encoder_outputs_fws = []
            encoder_outputs_bws = []

            with tf.variable_scope("enc"), tf.device(FLAGS.encoder_device):
                
                if FLAGS.use_fused_LSTM:
                    # fused LSTMs need time-major batches, e.g. [seq_len, batch_len,feat_len], so we transpose here
                    lstm_enc_input = tf.transpose(self.input_sequence, (1, 0, 2))
                    
                    if FLAGS.rnn_layers == 1:
                        # we have this special case for compatibility, the old model graphs dont have a variable scope for different LSTM layers
                        with tf.variable_scope("fw_fused"):
                            fw_lstm = tf.contrib.rnn.LSTMBlockFusedCell(FLAGS.rnn_hidden_cells, cell_clip=0, dtype=get_dtype(FLAGS.dtype))
                            encoder_outputs_fw, _ = fw_lstm(lstm_enc_input, dtype=get_dtype(FLAGS.dtype), sequence_length=self.input_sequence_length)
                            encoder_outputs_fws.append(encoder_outputs_fw)
                        with tf.variable_scope("bw_fused"):
                            bw_lstm = tf.contrib.rnn.TimeReversedFusedRNN(tf.contrib.rnn.LSTMBlockFusedCell(FLAGS.rnn_hidden_cells,
                                                                                                            cell_clip=0, dtype=get_dtype(FLAGS.dtype)))
                            encoder_outputs_bw, _ = bw_lstm(lstm_enc_input, dtype=get_dtype(FLAGS.dtype), sequence_length=self.input_sequence_length)
                            encoder_outputs_bws.append(encoder_outputs_bw)
                    else:          
                        for i in range(FLAGS.rnn_layers):
                            with tf.variable_scope("layer"+str(i)):
                                with tf.variable_scope("fw_fused"):
                                    fw_lstm = tf.contrib.rnn.LSTMBlockFusedCell(FLAGS.rnn_hidden_cells, cell_clip=0, dtype=get_dtype(FLAGS.dtype))
                                    encoder_outputs_fw, _ = fw_lstm(lstm_enc_input, dtype=get_dtype(FLAGS.dtype), sequence_length=self.input_sequence_length)
                                    encoder_outputs_fws.append(encoder_outputs_fw)

                                with tf.variable_scope("bw_fused"):
                                    bw_lstm = tf.contrib.rnn.TimeReversedFusedRNN(tf.contrib.rnn.LSTMBlockFusedCell(FLAGS.rnn_hidden_cells,
                                                                                                                    cell_clip=0, dtype=get_dtype(FLAGS.dtype)))
                                    encoder_outputs_bw, _ = bw_lstm(lstm_enc_input, dtype=get_dtype(FLAGS.dtype), sequence_length=self.input_sequence_length)
                                    encoder_outputs_bws.append(encoder_outputs_bw)
                                # new input to the next LSTM layer
                                lstm_enc_input = tf.concat([encoder_outputs_fw, encoder_outputs_bw], axis=2)
                else:
                    with tf.variable_scope("fw_std_lstm"):
                        encoder_cell_fw = tf.contrib.rnn.LSTMCell(FLAGS.rnn_hidden_cells, trainable=enc_trainable)
                        
                    with tf.variable_scope("bw_std_lstm"):
                        encoder_cell_bw = tf.contrib.rnn.LSTMCell(FLAGS.rnn_hidden_cells, trainable=enc_trainable)
                        
                    encoder_outputs, encoder_states = tf.nn.bidirectional_dynamic_rnn(encoder_cell_fw, encoder_cell_bw, 
                                                                                      self.input_sequence, sequence_length=self.input_sequence_length,
                                                                                      dtype=get_dtype(FLAGS.dtype))
                    encoder_outputs_fw, encoder_outputs_bw = encoder_outputs
                
                # If rnn_add_fw_bw is set, we 
                # Enc is the output of the encoder, enc_context is for the summary vector 
                # Summary vector = average encoding state across rnn_context_vector_layer, default 1st layer

                if FLAGS.rnn_add_fw_bw:
                    enc = encoder_outputs_fw + encoder_outputs_bw
                    enc_context = encoder_outputs_fws[FLAGS.rnn_context_vector_layer] + encoder_outputs_bws[FLAGS.rnn_context_vector_layer]

                else:
                    enc = tf.concat([encoder_outputs_fw, encoder_outputs_bw], axis=2)
                    enc_context = tf.concat([encoder_outputs_fws[FLAGS.rnn_context_vector_layer], encoder_outputs_bws[FLAGS.rnn_context_vector_layer]], axis=2)
                
                if FLAGS.use_fused_LSTM:
                    # transpose output back to batch major (since fused_LSTMs needs time-major input for efficiency)
                    enc = tf.transpose(enc, (1, 0, 2))
                    enc_context = tf.transpose(enc_context, (1, 0, 2))

                enc_out = slim.dropout(slim.fully_connected(enc, FLAGS.fc_size, normalizer_fn=None),
                                       keep_prob=FLAGS.dropout_keep_prob , is_training=is_training)

                print('enc_out shape:', enc_out.get_shape(), 'with dropout:', FLAGS.dropout_keep_prob, 'is_training:', is_training)
            
            with tf.device(FLAGS.bridge_device):
                self.query_out = enc_out
                self.bridge = enc_out
                
                self.sparsity_loss = tf.constant(0.0, get_dtype(FLAGS.dtype))
                self.diversity_loss = tf.constant(0.0, get_dtype(FLAGS.dtype))
                self.dwelling_loss = tf.constant(0.0, get_dtype(FLAGS.dtype))
                
                uniform_factor = 1.0/float(FLAGS.num_mem_entries)
                uniform_dist = tf.constant([1.0/float(FLAGS.num_mem_entries)] * FLAGS.num_mem_entries, get_dtype(FLAGS.dtype))
                
                self.eps = tf.constant(0.001, get_dtype(FLAGS.dtype))

                max_seq_len = tf.shape(enc_out)[1]
                
                if do_memory_augment:
                    
                    flattened_size = FLAGS.fc_size
                    
                    print('flattened_size:', flattened_size)
                    # Reshape conv output to fit fully connected layer input
                    enc_out_flattened = tf.reshape(enc_out, [-1, flattened_size])
                    print('enc_out flattened shape:' , enc_out_flattened.get_shape())
                    
                    context_mem_out, self.query_out, self.logits, sparsity_loss = memory_augment(enc_out_flattened, self.key_memory_W, self.key_memory_b, self.value_memory, print_tensor_dims=True,  
                                                                                    sparsity_regularizer_norm='infinity', pre_relu=False, euclidean_normalization=False,
                                                                                    softmax_type=FLAGS.memory_softmax_type, with_tanh=True if FLAGS.memory_softmax_type == 'gumbel_softmax' else False,
                                                                                    gumbel_temperature=(self.gumbel_temperature if FLAGS.memory_softmax_type == 'gumbel_softmax' else 1.0),
                                                                                    gumbel_noise_weight=(self.gumbel_noise_weight if FLAGS.memory_softmax_type == 'gumbel_softmax' else 0.0),
                                                                                                 dtype=get_dtype(FLAGS.dtype))
                    
                    if FLAGS.diversity_loss_type == 'simple':
                        diversity_loss = tf.reduce_sum(tf.reduce_max(self.query_out, axis=1))
                    elif FLAGS.diversity_loss_type == 'kl':
                        # This implements kl(self.query_out,[uniform_factor,uniform_factor,...,uniform_factor]) with softmax_cross_entropy_with_logits by substituion: kl(a,b) = softmax_cross_entropy_with_logits(a,a/b)
                        # The advantage of using softmax_cross_entropy_with_logits is numerical stability
                        # b needs to be non-negative though (as with kl)

                        y = self.query_out / uniform_factor
                        diversity_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.query_out,
                                                                                                labels=y))
                    elif FLAGS.diversity_loss_type == 'kl_neg':
                        y = self.query_out / uniform_factor
                        diversity_loss = tf.reduce_mean(-tf.nn.softmax_cross_entropy_with_logits(logits=self.query_out,
                                                                                                 labels=y))
                    elif FLAGS.diversity_loss_type == 'kl_log':
                        y = self.query_out / uniform_factor
                        diversity_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.logits,
                                                                                                labels=y))
                    elif FLAGS.diversity_loss_type == 'kl_neg_log':
                        y = self.query_out / uniform_factor
                        diversity_loss = tf.reduce_mean(-tf.nn.softmax_cross_entropy_with_logits(logits=self.logits,
                                                                                                 labels=y))
                    elif FLAGS.diversity_loss_type == 'kl_reverse':
                        diversity_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=uniform_dist,
                                                                                                labels=self.query_out*tf.reciprocal(self.query_out+self.eps)))
                    elif FLAGS.diversity_loss_type == 'kl_neg_reverse':
                        diversity_loss = tf.reduce_mean(-tf.nn.softmax_cross_entropy_with_logits(logits=uniform_dist,
                                                                                                 labels=self.query_out*tf.reciprocal(self.query_out+self.eps)))
                    else:
                        print('Error: ' + FLAGS.diversity_loss_type + ' is not supported. Must be either "simple" or "kl".')
                        sys.exit(-100)

                    dwelling_loss = tf.reduce_mean(tf.square(self.query_out[:-1] - self.query_out[:1]))
                    
                    self.bridge = tf.reshape(context_mem_out, [-1, max_seq_len, flattened_size])

                    self.query_out = tf.reshape(self.query_out, [-1, max_seq_len, FLAGS.num_mem_entries])
                    self.query_out_argmax = tf.argmax(self.query_out, axis=-1)
                    #self.query_out_argmax = tf.reshape(self.query_out_argmax, [-1, max_seq_len])
                
                len_norm = tf.expand_dims(tf.cast(self.input_sequence_length, dtype=get_dtype(FLAGS.dtype)), axis=1)
                # cheap (implicit) context vector, mean over the encoder outputs
                
                # choose enc or enc_out here
                if with_context_vector:
                    # expand context vector
                    context_vector = self.context_vector
                    if FLAGS.context_projection != -1:
                        context_vector = slim.fully_connected(context_vector, FLAGS.context_projection)
                    context_vector_expanded = tf.expand_dims(context_vector, axis=1)
                else:
                    context_vector = tf.reduce_sum(enc_context, axis=1) / len_norm
                    if FLAGS.context_projection != -1:
                        context_vector = slim.fully_connected(context_vector, FLAGS.context_projection)
                    context_vector_expanded = tf.expand_dims(context_vector, axis=1)
                
                print("context_vector_expanded shape:", context_vector_expanded.get_shape())
                print("self.bridge shape:", self.bridge.get_shape())
                
                self.bridge_without_concat = self.bridge
                if FLAGS.context_combine == 'add':
                    self.bridge += context_vector_expanded
                elif FLAGS.context_combine == 'concat':
                    context_vector_expanded_tiled = tf.tile(context_vector_expanded, [1, max_seq_len, 1])
                    print("context_vector_expanded_tiled shape:", context_vector_expanded_tiled.get_shape())
                    self.bridge = tf.concat([self.bridge, context_vector_expanded_tiled], axis=2)
                
                print("self.bridge shape:", self.bridge.get_shape())
                
                ##
                ## We apply dropout to the sequence, this means that some timestamps are blanked out from the encoder
                ## and the decoder needs to infer them on its own states
                ##
                self.bridge = tf.nn.dropout(self.bridge, keep_prob=FLAGS.bottleneck_dropout_keep_prob, 
                                            noise_shape=[tf.shape(self.bridge)[0], tf.shape(self.bridge)[1], 1])
            
            with tf.variable_scope("dec"), tf.device(FLAGS.decoder_device):
                
                if FLAGS.use_fused_LSTM:
                    #make bridge time-major
                    lstm_dec_input = tf.transpose(self.bridge, (1, 0, 2))
                    if FLAGS.rnn_layers == 1:
                        with tf.variable_scope("fw_fused"):
                            fw_lstm_dec = tf.contrib.rnn.LSTMBlockFusedCell(FLAGS.rnn_hidden_cells, cell_clip=0)
                            decoder_outputs_fw, _ = fw_lstm_dec(lstm_dec_input, dtype=get_dtype(FLAGS.dtype), sequence_length=self.input_sequence_length)
                        with tf.variable_scope("bw_fused"):
                            bw_lstm_dec = tf.contrib.rnn.TimeReversedFusedRNN(tf.contrib.rnn.LSTMBlockFusedCell(FLAGS.rnn_hidden_cells,
                                                                                                                cell_clip=0,
                                                                                                                dtype=get_dtype(FLAGS.dtype)))
                            decoder_outputs_bw, _ = bw_lstm_dec(lstm_dec_input, dtype=get_dtype(FLAGS.dtype), sequence_length=self.input_sequence_length)
                    else:
                        for i in range(FLAGS.rnn_layers):
                            with tf.variable_scope("layer"+str(i)):
                                with tf.variable_scope("fw_fused"):
                                    fw_lstm_dec = tf.contrib.rnn.LSTMBlockFusedCell(FLAGS.rnn_hidden_cells, cell_clip=0)
                                    decoder_outputs_fw, _ = fw_lstm_dec(lstm_dec_input, dtype=get_dtype(FLAGS.dtype), sequence_length=self.input_sequence_length)
                                with tf.variable_scope("bw_fused"):
                                    bw_lstm_dec = tf.contrib.rnn.TimeReversedFusedRNN(tf.contrib.rnn.LSTMBlockFusedCell(FLAGS.rnn_hidden_cells,
                                                                                                                        cell_clip=0,
                                                                                                                        dtype=get_dtype(FLAGS.dtype)))
                                    decoder_outputs_bw, _ = bw_lstm_dec(lstm_dec_input, dtype=get_dtype(FLAGS.dtype), sequence_length=self.input_sequence_length)
                                lstm_dec_input = tf.concat([decoder_outputs_fw, decoder_outputs_bw], axis=2)
                else:
                    with tf.variable_scope("fw_std_lstm"):
                        decoder_cell_fw = tf.contrib.rnn.LSTMCell(FLAGS.rnn_hidden_cells)
                    with tf.variable_scope("bw_std_lstm"):
                        decoder_cell_bw = tf.contrib.rnn.LSTMCell(FLAGS.rnn_hidden_cells)
                    
                    decoder_outputs, decoder_states = tf.nn.bidirectional_dynamic_rnn(decoder_cell_fw, decoder_cell_bw, self.bridge, 
                                                                                      sequence_length=self.input_sequence_length, dtype=get_dtype(FLAGS.dtype))
                    decoder_outputs_fw, decoder_outputs_bw = decoder_outputs
                
                if FLAGS.rnn_add_fw_bw:
                    dec = decoder_outputs_fw + decoder_outputs_bw
                else:
                    dec = tf.concat([decoder_outputs_fw, decoder_outputs_bw], axis=2)
                
                if FLAGS.use_fused_LSTM:
                    #transpose back from time-major to batch-major
                    dec = tf.transpose(dec, (1, 0, 2))
                    
                if FLAGS.context_combine == 'post_add':
                    dec += context_vector_expanded
                elif FLAGS.context_combine == 'post_concat':
                    context_vector_expanded_tiled = tf.tile(context_vector_expanded, [1,max_seq_len,1])
                    print("context_vector_expanded_tiled shape:", context_vector_expanded_tiled.get_shape())
                    dec = tf.concat([dec, context_vector_expanded_tiled], axis=2)
                            
                dec_out = slim.fully_connected(dec, feat_size, activation_fn=None, normalizer_fn=None)
                print('dec_out shape:', dec_out.get_shape(), 'with dropout:', FLAGS.dropout_keep_prob, 'is_training:', is_training)
            
            self.enc = enc
            self.enc_out = enc_out
            self.dec = dec
            
            self.out = dec_out
            #encoder_outputs is [batch_size, max_time, cell_fw.output_size]
            
            #encoder_output_state_fw, encoder_output_state_bw = encoder_states
        
            # In a dynamic bi-lstm, output_state_fw and output_state_bw will have the last states of the lstm accross the batch.
            # The output is a tuple (C,h) and we need h. Output is [batch_size, cell.output_size] 
                        
            #lstm_encoder_out = output_state_fw[1] + output_state_bw[1]
            with tf.device(FLAGS.loss_device): 
                if FLAGS.loss == 'rms':
                    length_norm = tf.cast(tf.reduce_sum(self.input_sequence_length), dtype=get_dtype(FLAGS.dtype))
                    if do_memory_augment:
                        self.diversity_loss = tf.cast(FLAGS.diversity_regularizer_scale, dtype=get_dtype(FLAGS.dtype)) * tf.cast(diversity_loss, dtype=get_dtype(FLAGS.dtype)) / length_norm
                        self.sparsity_loss = tf.cast(self.sparsity_weight, dtype=get_dtype(FLAGS.dtype)) * sparsity_loss / length_norm
                        # this one already used reduce_mean for the reduction, so no length norm is needed
                        self.dwelling_loss = tf.cast(FLAGS.dwelling_regularizer_scale, dtype=get_dtype(FLAGS.dtype)) * dwelling_loss
                    #self.cost = (tf.reduce_sum(kl(dec_out+self.eps, self.output_sequence+self.eps) + kl(self.output_sequence+self.eps, dec_out+self.eps)))
                    print("length norm", length_norm)
                    print("self.diversity_loss:", self.diversity_loss)
                    print("self.sparsity_loss:", self.sparsity_loss)
                    print("self.dwelling_loss:",self.dwelling_loss)
                    print("dec_out",dec_out)
                    self.cost = (tf.reduce_sum(tf.square(dec_out - self.output_sequence)) / length_norm)
                    print("cost",self.cost)
                    self.cost += self.sparsity_loss + self.diversity_loss + self.dwelling_loss
          
            #if is_training:
            #    self.create_training_graphs(create_new_train_dir)
            #else:
            #    self.create_training_graphs(create_new_train_dir=False)

            self.saver = tf.train.Saver(tf.global_variables(), max_to_keep=FLAGS.max_to_keep_models)
        
    def create_training_graphs(self, clip_norm=True, max_grad_norm=5.0):
        # Define Training procedure
        self.global_step = tf.Variable(0, name="global_step", trainable=False)
        self.optimizer = tf.train.AdamOptimizer(FLAGS.learn_rate, FLAGS.adam_epsilon) #tf.train.RMSPropOptimizer(FLAGS.learn_rate) #tf.train.AdamOptimizer(FLAGS.learn_rate)

        self.train_vars = tf.trainable_variables()  
        
        print("Creating train_op for:", self.train_vars)
        
        self.train_op = slim.learning.create_train_op(self.cost, self.optimizer, global_step=self.global_step, colocate_gradients_with_ops=True,
                                                      clip_gradient_norm=FLAGS.gradient_clipping, variables_to_train=self.train_vars)
        
        #if self.do_memory_augment:
        #    self.debug_key_memory_grads = tf.reduce_sum(tf.gradients(self.cost, self.key_memory)[0])

    def create_summary_and_train_dir(self, create_new_train_dir=True, resumed=False):
        if create_new_train_dir:
            timestamp = str(int(time.time()))
            self.out_dir = os.path.abspath(os.path.join(FLAGS.train_dir, "runs" if not resumed else "resumed",
                                                        timestamp + get_model_flags_param_short())) + '/' + 'tf10'
            print("Writing to {}\n".format(self.out_dir))
            # Checkpoint directory. Tensorflow assumes this directory already exists so we need to create it
            checkpoint_dir = os.path.abspath(os.path.join(self.out_dir, "checkpoints"))
            #checkpoint_prefix = os.path.join(checkpoint_dir, "model")
            if not os.path.exists(checkpoint_dir):
                os.makedirs(checkpoint_dir)
            with open(self.out_dir + 'params','w') as param_file:
                param_file.write(get_FLAGS_params_as_str()+'\n')
        else:
            self.out_dir = FLAGS.train_dir 

        if FLAGS.log_tensorboard:   
            loss_summary = tf.summary.scalar('loss', self.cost)         

            self.train_summary_op = tf.summary.merge_all()
            train_summary_dir = os.path.join(self.out_dir, "summaries", "train")
            
    # do a training step with the supplied input data
    def step(self, sess, input_sequence, input_sequence_length, output_sequence, gumbel_temperature=1.0,
             gumbel_noise_weight=0.0, context_vector=None, sparsity_weight=0.0):
        feed_dict = {self.input_sequence: input_sequence, self.input_sequence_length: input_sequence_length,
                     self.output_sequence: output_sequence, self.sparsity_weight: sparsity_weight,
                     self.gumbel_temperature: gumbel_temperature, self.gumbel_noise_weight: gumbel_noise_weight}
        
        if context_vector != None and context_vector != []:
            feed_dict[self.context_vector] = context_vector

        tensor_out = sess.run([self.train_op, self.out, self.cost, self.sparsity_loss, self.diversity_loss, self.dwelling_loss,
                               self.dec, self.enc, self.enc_out, self.query_out, self.bridge_without_concat], feed_dict=feed_dict)
        #print(tensor_out)
        _, output, loss, sparsity_loss, diversity_loss, dwelling_loss, dec, enc, enc_out, query_out, bridge = tensor_out
        return  output, loss, sparsity_loss, diversity_loss, dwelling_loss, dec, enc, enc_out, query_out, bridge

def dtw_distance(x, y, normalized):
    """Dynamic time warping cosine distance
    The "feature" dimension is along the columns and the "time" dimension
    along the lines of arrays x and y
    """
    if x.shape[0] > 0 and y.shape[0] > 0:
        # x and y are not empty
        d = dtw.dtw(x, y, cosine.cosine_distance,normalized)
    elif x.shape[0] == y.shape[0]:
        # both x and y are empty
        d = 0
    else:
        # x or y is empty
        d = np.inf
    return d


def dtw_kl_divergence(x, y, normalized):
    """Kullback-Leibler divergence"""
    if x.shape[0] > 0 and y.shape[0] > 0:
        d = dtw.dtw(x, y, kullback_leibler.kl_divergence, normalized)
    elif x.shape[0] == y.shape[0]:
        d = 0
    else:
        d = np.inf
    return d

def evaluate_NMI(train_dir, curriculum_utt_id_list, idlist_size, results_file="results_file.csv", compute_feat_baseline=False):
    use_context_features = (FLAGS.filelist_context_features != '')
    with tf.device(FLAGS.device):
        with tf.Session(config=tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)) as sess: 
            # we have to create the graph excatly as in train, otherwise the saver will have problems restoring 
            with tf.variable_scope("sparse_speech_model"):
                pre_train_model = SparseSpeech(batch_size=FLAGS.batch_size, do_memory_augment=False, with_context_vector=use_context_features)
            with tf.variable_scope("sparse_speech_model", reuse=True): 
                model = SparseSpeech(batch_size=FLAGS.batch_size, do_memory_augment=True, with_context_vector=use_context_features)
                          
            # model.create_training_graphs()
            if train_dir.endswith("tf10"):
                train_dir = train_dir[:-4]
            if train_dir != "":
                ckpt = tf.train.get_checkpoint_state(train_dir)
                if ckpt and ckpt.model_checkpoint_path:
                    sess.run(tf.global_variables_initializer())
                    print("Reading model parameters from %s" % ckpt.model_checkpoint_path)
                    model.saver.restore(sess, ckpt.model_checkpoint_path)
                else:
                    print("Error, couldnt open:", train_dir)
                    sys.exit(-1)
        
            query_outs = []
            query_lens = []
            batch_num = 0
            for i in range(0,idlist_size,FLAGS.batch_size):
                data_batch, len_batch, context_features = get_batch_ordered(i, FLAGS.batch_size, curriculum_utt_id_list,
                                                                            idlist_size, context_feature=use_context_features)
                
                if use_context_features:
                    query_out_argmaxes = sess.run(model.query_out_argmax , feed_dict={model.input_sequence: data_batch,
                                                                                      model.input_sequence_length: len_batch,
                                                                                      model.context_features: context_features})
                else:
                    query_out_argmaxes = sess.run(model.query_out_argmax , feed_dict={model.input_sequence: data_batch,
                                                                                      model.input_sequence_length: len_batch})
                
                # iterate over output batch, then add the sequences of the batch one by one, but only up until the true query_length
                for query_out_argmax, query_len in  zip(query_out_argmaxes, len_batch):
                     query_outs += list(query_out_argmax[:query_len])
                     query_lens += [query_len]
                batch_num += 1
                
            print('Processed', batch_num, 'input batches.')
            
    len_query_outs = len(query_outs)
    print('query outs length:', len_query_outs)
    
    # is there a one off error here?
    alignment_rasterized = []
    for elem,query_len in zip(curriculum_utt_id_list, query_lens):
        alignment_lens = 0
        for start,end,intid in alignment_data[elem]:
            intid = int(intid)
            length = end - start
            alignment_rasterized += [phones_reduced_trans_data[intid]]*length
            alignment_lens += length
   
        if alignment_lens != query_len:
            # there seems to be a rounding error in feature generation
            # (or the alignment giving dropping the last frame in a couple of instances?)
            alignment_lens -= 1
            alignment_rasterized = alignment_rasterized[:-1]
            
        if alignment_lens != query_len:   
            print("lens differ in", elem ,":", alignment_lens, "and query", query_len)
    
    len_alignment_rasterized = len(alignment_rasterized)
    
    print('query outs length:', len_query_outs)
    print('alignment_rasterized length:', len_alignment_rasterized)
    
    assert(len_query_outs == len_alignment_rasterized)
    
    max_unit_discovered = max(query_outs) + 1
    max_unit_alignments = max(alignment_rasterized) + 1
    
    print("max_unit_discovered:", max_unit_discovered, "max_unit_alignments:", max_unit_alignments)
    
    if FLAGS.pylab_show_feats:
        print("Co-occurence matrix:")
        co_occurence = np.zeros((max_unit_discovered, max_unit_alignments), dtype=np.int64)
        
        for unit_discovered, unit_alignments in zip(query_outs, alignment_rasterized):
            co_occurence[unit_discovered][unit_alignments] += 1
            
        plt.imshow(co_occurence.T, interpolation=None, aspect='auto', origin='lower')
        plt.show()
        
        if train_dir[-1] == '/':
            train_dir = train_dir[:-1]
        
        if compute_feat_baseline:
            modelname = train_dir
        else:    
            modelname = train_dir.split("/")[-1]
    
    # we evaluate the loaded model now:
    ARI = metrics.adjusted_rand_score(alignment_rasterized, query_outs)
    print('ARI score:', number_format % ARI)
    NMI = metrics.v_measure_score(alignment_rasterized, query_outs)
    print('NMI / V-measure:', number_format % NMI)
    
    myutils.save_result_csv(results_file, feat_key=modelname, result_key="ARI", result=(number_format % ARI), 
                            results_table_header_dict=results_table_header_dict, results_table_header=results_table_header)
    myutils.save_result_csv(results_file, feat_key=modelname, result_key="NMI", result=(number_format % NMI),
                            results_table_header_dict=results_table_header_dict, results_table_header=results_table_header)


def write_unsup_challenge_feat(train_dir, curriculum_utt_id_list, only_pretrain=False, bridge_without_concat=False,
                               eps=1.0e-7, kaldi_feat=False, kaldi_output_text_file='text'):
    
    use_context_features = (FLAGS.filelist_context_features != '')
    
    with tf.device(FLAGS.device):
        with tf.Session(config=tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)) as sess:    
            
            print('Starting feature generation...')
            if only_pretrain:
                print('Using pre train model.')
            
            if not FLAGS.gen_bypass:
                # we have to create the graph excatly as in train, otherwise the saver will have problems restoring 
                with tf.variable_scope("sparse_speech_model"):
                    pre_train_model = SparseSpeech(batch_size=FLAGS.batch_size, do_memory_augment=False,
                                                   with_context_vector=use_context_features)
                    
                if not only_pretrain:
                    with tf.variable_scope("sparse_speech_model", reuse=True): 
                        model = SparseSpeech(batch_size=FLAGS.batch_size, do_memory_augment=True,
                                             with_context_vector=use_context_features)
                else:
                    model = pre_train_model
                
                if train_dir.endswith("tf10"):
                    train_dir = train_dir[:-4]
                if train_dir != "":
                    ckpt = tf.train.get_checkpoint_state(train_dir)
                    if ckpt and ckpt.model_checkpoint_path:
                        sess.run(tf.global_variables_initializer())
                        print("Reading model parameters from %s" % ckpt.model_checkpoint_path)
                        model.saver.restore(sess, ckpt.model_checkpoint_path)
                    else:
                        print("Error, couldnt open:", train_dir)
                        sys.exit(-1)
        
            idlist_size = len(curriculum_utt_id_list)                
        
            if kaldi_feat:
                i=0
                with open(kaldi_output_text_file, 'w') as kaldi_out:
                    while(i<idlist_size):
                        print('Generating batch...')
                        data_batch, len_batch, context_features, ids = get_batch_ordered(i, FLAGS.batch_size, curriculum_utt_id_list,
                                                                                         idlist_size, context_feature=use_context_features,
                                                                                         return_ids=True)
                        i+=FLAGS.batch_size
                        
                        result_tensor = model.query_out_argmax
                        if use_context_features:
                            query_out = sess.run(result_tensor, feed_dict={model.input_sequence: data_batch,
                                                                           model.input_sequence_length: len_batch,
                                                                           model.context_vector: context_features,
                                                                           model.gumbel_temperature:FLAGS.gumbel_generate_temperature})
                        else:
                            query_out = sess.run(result_tensor, feed_dict={model.input_sequence: data_batch,
                                                                           model.input_sequence_length: len_batch,
                                                                           model.gumbel_temperature:FLAGS.gumbel_generate_temperature})
                        
                        for myid,query,_len in zip(ids,query_out,len_batch):
                            # collapse consecutive elements into one
                            symbol_seq = [x[0] for x in groupby(list(query[:_len]))]
                            
                            print('Writing', myid)
                            kaldi_out.write(myid + ' ' + ' '.join([str(sym) for sym in symbol_seq]) + '\n')
                            
                return
            
            if FLAGS.gen_zerospeech_dir is not None and FLAGS.gen_zerospeech_dir != '':
                time_start = 125
                time_inc = 100
                
            
                myutils.ensure_dir(FLAGS.gen_zerospeech_dir + '/1s/')
                myutils.ensure_dir(FLAGS.gen_zerospeech_dir + '/10s/')
                myutils.ensure_dir(FLAGS.gen_zerospeech_dir + '/120s/')
                
                i=0
                while(i<idlist_size):
                   
                    data_batch, len_batch, context_features, ids = get_batch_ordered(i, FLAGS.batch_size, curriculum_utt_id_list, idlist_size,
                                                                                     context_feature=use_context_features, return_ids=True)
                     
                    i+=FLAGS.batch_size
                    
                    if not FLAGS.gen_bypass:
                        if only_pretrain:
                            result_tensor = model.enc_out
                        elif bridge_without_concat:
                            result_tensor = model.bridge_without_concat
                        else:    
                            result_tensor = model.query_out
                    
                    if FLAGS.gen_bypass:
                        query_out = data_batch
                    else:
                        if use_context_features:
                            query_out = sess.run(result_tensor, feed_dict={model.input_sequence: data_batch, model.input_sequence_length: len_batch, 
                                                                           model.context_vector: context_features,
                                                                           model.gumbel_temperature:FLAGS.gumbel_generate_temperature})
                        else:
                            query_out = sess.run(result_tensor, feed_dict={model.input_sequence: data_batch, model.input_sequence_length: len_batch, 
                                                                           model.gumbel_temperature:FLAGS.gumbel_generate_temperature})
                    
                    for myid,query,_len in zip(ids,query_out,len_batch):
                        current_time = time_start
                        filename = FLAGS.gen_zerospeech_dir + '/' + myid.replace('.wav','.fea').replace('_','/')
                        with open(filename, 'w') as feat_out:
                            print('Writing' + filename)
                            query = query[:_len]
                            for feat in query:                      
                                time_out = current_time / 10000.0
                                if bridge_without_concat:
                                    feat_out.write("%.4f" % time_out + ' ' + ' '.join(str(elem) for elem in feat) + '\n')
                                else:
                                    feat_out.write("%.4f" % time_out + ' ' + ' '.join(str(elem) if abs(elem) > eps else "0.0" for elem in feat) + '\n')
                                current_time += time_inc

# ABX
def evaluate_ABX(train_dir, curriculum_utt_id_list, idlist_size, phn_trigram=True, results_file_across="results_file_ABX_across.csv",
                 results_file_within="results_file_ABX_within.csv", debug_print=False, random_seed=42, compute_feat_baseline=False,
                 DPGMM_baseline_model=None, dtw_normalized=False):
    # to make the exact order reproducible
    random_instance = random.Random(random_seed)
    use_context_features = (FLAGS.filelist_context_features != '')
    with tf.device(FLAGS.device):
        with tf.Session(config=tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)) as sess:    
            
            print('Starting ABX evaluation.')
            
            if not compute_feat_baseline:
                # we have to create the graph excatly as in train, otherwise the saver will have problems restoring 
                with tf.variable_scope("sparse_speech_model"):
                    pre_train_model = SparseSpeech(batch_size=FLAGS.batch_size, do_memory_augment=False, with_context_vector=use_context_features)
                with tf.variable_scope("sparse_speech_model", reuse=True): 
                    model = SparseSpeech(batch_size=FLAGS.batch_size, do_memory_augment=True, with_context_vector=use_context_features)
                
                if train_dir.endswith("tf10"):
                    train_dir = train_dir[:-4]
                if train_dir != "":
                    ckpt = tf.train.get_checkpoint_state(train_dir)
                    if ckpt and ckpt.model_checkpoint_path:
                        sess.run(tf.global_variables_initializer())
                        print("Reading model parameters from %s" % ckpt.model_checkpoint_path)
                        model.saver.restore(sess, ckpt.model_checkpoint_path)
                    else:
                        print("Error, couldnt open:", train_dir)
                        sys.exit(-1)
            
            correct = 0
            not_correct = 0
            kl_correct = 0
            kl_not_correct = 0
            dtw_samples = 0
            within_dtw_samples = 0

            abx_evals = 0

            within_correct = 0
            within_not_correct = 0
            within_kl_correct = 0
            within_kl_not_correct = 0
            
            for i in xrange(FLAGS.ABX_eval_samples):
                
                if debug_print:
                    print("Computing eval sample:", i)
                
                spklist = list(spk2utt_data.keys())
                random_instance.shuffle(spklist)
                
                ABspks = spklist[:2]
                
                if debug_print:
                    print("Selected spks:", ABspks)
                    
                if phn_trigram:
                    Aphns = [phones_reduced_trans_data[int(elem[2])] for uttid in spk2utt_data[ABspks[0]]
                             for elem in alignment_data[uttid]]
                    Bphns = [phones_reduced_trans_data[int(elem[2])] for uttid in spk2utt_data[ABspks[0]]
                             for elem in alignment_data[uttid]]
                    Awords_tri_phns = zip(Aphns, Aphns[1:], Aphns[2:])
                    Bwords_tri_phns = zip(Bphns, Bphns[1:], Bphns[2:])
                    Awords_set = set(Awords_tri_phns)
                    Bwords_set = set(Bwords_tri_phns)
                    SIL_index = phones_reduced.index('SIL')
                    SPN_index = -1
                    if 'SPN' in phones_reduced:
                        SPN_index = phones_reduced.index('SPN')
                else:
                    #words
                    Awords_set = set(elem[2] for uttid in spk2utt_data[ABspks[0]] for elem in word_alignment_data[uttid])
                    Bwords_set = set(elem[2] for uttid in spk2utt_data[ABspks[1]] for elem in word_alignment_data[uttid])
                
                spk_a_utts = spk2utt_data[ABspks[0]]
                spk_b_utts = spk2utt_data[ABspks[1]]
                
                if debug_print:
                    print(spk_a_utts)
                    print(spk_b_utts)
                
                if phn_trigram:
                    spk_a_phn_alignment_data = [(uttid,start,end,phones_reduced_trans_data[int(phn)]) for uttid in spk2utt_data[ABspks[0]]
                                                for start,end,phn in alignment_data[uttid]]
                    spk_b_phn_alignment_data = [(uttid,start,end,phones_reduced_trans_data[int(phn)]) for uttid in spk2utt_data[ABspks[1]]
                                                for start,end,phn in alignment_data[uttid]]
                    
                    # Ok. These two lines look a bit crazy, I know, but basically align[i][0] = uttid
                    # and align[i][1] = start and align[i][2] = end and and align[i][3] = int(phn)
                    # We do trigrams over the (uttid,start,end,int(phn)) quatruples and
                    # then we regroup them, so that start = start of the trigram and end = end
                    # of the trigram and we have (uttid,start,end,(phn1,phn2,phn3))
                    # Note: We also need to be careful that we don't cross utterance boundaties
                    #  (the if align... part of the list comprehension)

                    spk_a_alignment_data = [(align[0][0], align[0][1], align[2][2], (align[0][3], align[1][3], align[2][3]))
                                            for align in zip(spk_a_phn_alignment_data, spk_a_phn_alignment_data[1:],
                                                             spk_a_phn_alignment_data[2:]) if align[0][0] == align[1][0] and align[1][0] == align[2][0]]
                    spk_b_alignment_data = [(align[0][0], align[0][1], align[2][2], (align[0][3], align[1][3], align[2][3]))
                                            for align in zip(spk_b_phn_alignment_data, spk_b_phn_alignment_data[1:],
                                                             spk_b_phn_alignment_data[2:]) if align[0][0] == align[1][0] and align[1][0] == align[2][0]]
                    #print(spk_a_alignment_data)
                else:
                    spk_a_alignment_data = [(uttid, start, end, word) for uttid in spk2utt_data[ABspks[0]]
                                            for start, end, word in word_alignment_data[uttid]]
                    spk_b_alignment_data = [(uttid, start, end, word) for uttid in spk2utt_data[ABspks[1]]
                                            for start, end, word in word_alignment_data[uttid]]
                
                #print(spk_a_alignment_data)
                #print(spk_b_alignment_data)
                
                common_words = list(Awords_set.intersection(Bwords_set))
                
                if debug_print:
                    print('common_words:', list(common_words))
                
                if len(common_words) == 0:
                    continue
                
                data_batch_A, len_batch_A, context_features_A = get_batch_specific(spk_a_utts, context_feature=use_context_features)
                data_batch_B, len_batch_B, context_features_B = get_batch_specific(spk_b_utts, context_feature=use_context_features)
            
                if compute_feat_baseline:
                    if DPGMM_baseline_model is None:
                        query_out_A = data_batch_A
                        query_out_B = data_batch_B
                    else:
                        query_out_A, query_out_B = [],[]
                        for a in data_batch_A:
                            query_out_A += [DPGMM_baseline_model.predict_proba(a)]
                        for b in data_batch_B:
                            query_out_B += [DPGMM_baseline_model.predict_proba(b)]
                else:
                    if use_context_features:
                        query_out_A = sess.run(model.query_out, feed_dict={model.input_sequence: data_batch_A, model.input_sequence_length: len_batch_A,
                                                                           model.context_vector: context_features_A,
                                                                           model.gumbel_temperature:FLAGS.gumbel_generate_temperature,
                                                                           model.gumbel_noise_weight:0.0})
                        query_out_B = sess.run(model.query_out, feed_dict={model.input_sequence: data_batch_B,
                                                                           model.input_sequence_length: len_batch_B,
                                                                           model.context_vector: context_features_B,
                                                                           model.gumbel_temperature:FLAGS.gumbel_generate_temperature,
                                                                           model.gumbel_noise_weight:0.0})
                    else:
                        query_out_A = sess.run(model.query_out, feed_dict={model.input_sequence: data_batch_A, model.input_sequence_length: len_batch_A, 
                                                                           model.gumbel_temperature:FLAGS.gumbel_generate_temperature,
                                                                           model.gumbel_noise_weight:0.0})
                        query_out_B = sess.run(model.query_out, feed_dict={model.input_sequence: data_batch_B, model.input_sequence_length: len_batch_B,
                                                                           model.gumbel_temperature:FLAGS.gumbel_generate_temperature,
                                                                           model.gumbel_noise_weight:0.0})
                        
                for equal_word in common_words:
                    
                    if phn_trigram:
                        # don't compare silence
                        if equal_word[0] == SIL_index or equal_word[1] == SIL_index or equal_word[2] == SIL_index or \
                                equal_word[0] == SPN_index or equal_word[1] == SPN_index or equal_word[2] == SPN_index:
                            continue
                    
                    if debug_print:
                        if phn_trigram:
                            print('equal_word', [phones_reduced[int(phn)] for phn in equal_word])
                        else:
                            print('equal_word', equal_word)

                    equal_word_spkAs_ali_data = [elem for elem in spk_a_alignment_data if elem[3] == equal_word]
                    equal_word_spkBs_ali_data = [elem for elem in spk_b_alignment_data if elem[3] == equal_word]
                    
                    if phn_trigram:
                        # all other trigrams where only the center phone is different
                        diff_word_spkAs_ali_data = [elem for elem in spk_a_alignment_data if elem[3][0] == equal_word[0]
                                                    and elem[3][1] != equal_word[1] and elem[3][2] == equal_word[2] 
                                                    and elem[3][1] != SIL_index and elem[3][1] != SPN_index]
                    else:
                        diff_word_spkAs_ali_data = [elem for elem in spk_a_alignment_data if elem[3] != equal_word]

                    if debug_print:
                        print(equal_word_spkAs_ali_data)

                    if len(equal_word_spkAs_ali_data) < 1 or len(equal_word_spkBs_ali_data) < 1 \
                            or len(diff_word_spkAs_ali_data) < 2:
                        if debug_print:
                            print('Skipping, data:', equal_word, len(equal_word_spkAs_ali_data),
                                  len(equal_word_spkBs_ali_data), len(diff_word_spkAs_ali_data))
                        continue

                    random_instance.shuffle(equal_word_spkAs_ali_data)
                    random_instance.shuffle(equal_word_spkBs_ali_data)
                    
                    common_ali_data_min_len = min(len(equal_word_spkAs_ali_data),len(equal_word_spkBs_ali_data))
                    
                    # ABX within speakers       
                    if len(equal_word_spkAs_ali_data) > 1:
                        all_combinations = list(itertools.combinations(equal_word_spkAs_ali_data[:60], r=2))
                        random_instance.shuffle(all_combinations)
                        for pair in all_combinations[:common_ali_data_min_len]:
                            diff_word_spkA = random.choice(diff_word_spkAs_ali_data)

                            uttid_equal_a = pair[0][0]
                            uttid_equal_a_2 = pair[1][0]
                            uttid_diff_a = diff_word_spkA[0]
                            
                            query_word_a_equal_index = spk_a_utts.index(uttid_equal_a)
                            query_word_a_2_equal_index = spk_a_utts.index(uttid_equal_a_2)
                            query_word_a_diff_index = spk_a_utts.index(uttid_diff_a)

                            queryout_A = query_out_A[query_word_a_equal_index][pair[0][1]:pair[0][2]]
                            queryout_X = query_out_A[query_word_a_2_equal_index][pair[1][1]:pair[1][2]]
                            queryout_B = query_out_A[query_word_a_diff_index][diff_word_spkA[1]:diff_word_spkA[2]]
                    
                            dtw_equal_kl = dtw_kl_divergence(queryout_A, queryout_X , normalized=dtw_normalized) 
                            dtw_diff_kl = dtw_kl_divergence(queryout_B, queryout_X, normalized=dtw_normalized)
                            
                            dtw_equal_dtw = dtw_distance(queryout_A, queryout_X, normalized=dtw_normalized) 
                            dtw_diff_dtw = dtw_distance(queryout_B, queryout_X, normalized=dtw_normalized)    
                            
                            if debug_print:
                                
                                f, axarr = plt.subplots(3, sharex=True) #, figsize=(28, 14))
                      
                                for i,ax in enumerate(axarr):
                                    if i==0:
                                        ax.imshow(queryout_A.T, interpolation=None, aspect='auto', origin='lower')
                                    if i==1:
                                        ax.imshow(queryout_X.T, interpolation=None, aspect='auto', origin='lower')
                                    if i==2:
                                        ax.imshow(queryout_B.T, interpolation=None, aspect='auto', origin='lower')
                                
                                plt.show()
                                
                                if phn_trigram:
                                    print('within DTW equal word dist',[phones_reduced[int(phn)] for phn in equal_word],
                                          'A/A:',dtw_equal_dtw)
                                    print('within DTW diff word dist',[phones_reduced[int(phn)] for phn in equal_word], 
                                          '/',[phones_reduced[int(phn)] for phn in diff_word_spkA[3]],
                                          'A/A_diff:',dtw_diff_dtw)
                                else:
                                    print('within DTW equal word dist', equal_word,'A/X:',dtw_equal_dtw)
                                    print('within DTW diff word dist',equal_word,'A_diff',diff_word_spkA[3],'B/X:',dtw_diff_dtw)
                                    #print()
                            
                            if dtw_equal_dtw < dtw_diff_dtw:
                                within_correct += 1
                                if debug_print:
                                    print('correct')
                            else:
                                within_not_correct += 1
                                if debug_print:
                                    print('in_correct')
                            
                            if dtw_equal_kl < dtw_diff_kl:
                                within_kl_correct += 1
                                if debug_print:
                                    print('correct')
                            else:
                                within_kl_not_correct += 1
                                if debug_print:
                                    print('in_correct')
                            within_dtw_samples += 1
                            #if len(equal_word_spkBs_ali_data) > 1:
                            #    for pair in itertools.combinations(equal_word_spkBs_ali_data):
                            #        random_instance.shuffle(diff_word_spkBs_ali_data)
                            
                    # ABX across speakers
                    for equal_word_spkA, equal_word_spkB in zip(equal_word_spkAs_ali_data, equal_word_spkBs_ali_data):
                        random_instance.shuffle(diff_word_spkAs_ali_data)
                        diff_word_spkA = diff_word_spkAs_ali_data[0]
                        
                        uttid_equal_a = equal_word_spkA[0]
                        uttid_equal_b = equal_word_spkB[0]
                        uttid_diff_a = diff_word_spkA[0]
                        
                        query_word_a_equal_index = spk_a_utts.index(uttid_equal_a)
                        query_word_b_equal_index = spk_b_utts.index(uttid_equal_b)
                        query_word_a_diff_index = spk_a_utts.index(uttid_diff_a)
                        
                        queryout_A = query_out_A[query_word_a_equal_index][equal_word_spkA[1]:equal_word_spkA[2]]
                        queryout_X = query_out_B[query_word_b_equal_index][equal_word_spkB[1]:equal_word_spkB[2]]
                        queryout_B = query_out_A[query_word_a_diff_index][diff_word_spkA[1]:diff_word_spkA[2]]
                    
                        if debug_print:
                            print("len(queryout_A_equal):", len(queryout_A), "len(queryout_B_equal):",
                                  len(queryout_X), " len(queryout_A_diff):", len(queryout_B))
                    
                        dtw_equal_kl = dtw_kl_divergence(queryout_A, queryout_X, normalized=dtw_normalized) 
                        dtw_diff_kl = dtw_kl_divergence(queryout_B, queryout_X, normalized=dtw_normalized)
                        
                        dtw_equal_dtw = dtw_distance(queryout_A, queryout_X, normalized=dtw_normalized)
                        dtw_diff_dtw =dtw_distance(queryout_B, queryout_X, normalized=dtw_normalized)
                        
                        if debug_print:
                            
                            f, axarr = plt.subplots(3, sharex=True) #, figsize=(28, 14))
                      
                            for i,ax in enumerate(axarr):
                                if i==0:
                                    ax.imshow(queryout_A.T, interpolation=None, aspect='auto', origin='lower')
                                if i==1:
                                    ax.imshow(queryout_X.T, interpolation=None, aspect='auto', origin='lower')
                                if i==2:
                                    ax.imshow(queryout_B.T, interpolation=None, aspect='auto', origin='lower')
                                
                            plt.show()
                            
                            print('across DTW equal word dist',equal_word,'A/B:',dtw_equal_dtw)
                            if phn_trigram:
                                print('across DTW diff word dist',[phones_reduced[int(phn)] for phn in equal_word],
                                                                '/',[phones_reduced[int(phn)] for phn in diff_word_spkA[3]],'A/A:',dtw_diff_dtw)
                            else:
                                print('across DTW diff word dist',equal_word,'/',diff_word_spkA[3],'A/A:',dtw_diff_dtw)
                        
                        dtw_samples += 1
                        
                        if dtw_equal_dtw < dtw_diff_dtw:
                            correct += 1
                            if debug_print:
                                print('correct')
                        else:
                            not_correct += 1
                            if debug_print:
                                print('in_correct')
                            
                        if dtw_equal_kl < dtw_diff_kl:
                            kl_correct += 1
                            if debug_print:
                                print('correct')
                        else:
                            kl_not_correct += 1
                            if debug_print:
                                print('in_correct')
                
                if not_correct + correct > 0:
                    ABX_err = float(not_correct) / float(not_correct + correct)
                    ABX_err_kl = float(kl_not_correct) / float(kl_not_correct + kl_correct)
                    within_ABX_err = float(within_not_correct) / float(within_not_correct + within_correct)
                    within_ABX_err_kl = float(within_kl_not_correct) / float(within_kl_not_correct + within_kl_correct)
                    print('After', dtw_samples, 'across samples and',within_dtw_samples,
                          'within samples, abx_evals:',abx_evals,', across ABX error:', ABX_err)
                    print('After', dtw_samples, 'across samples and',within_dtw_samples,
                          'within samples, abx_evals:',abx_evals,', across ABX-kl error:', ABX_err_kl)
                    print('After', dtw_samples, 'across samples and',within_dtw_samples,
                          'within samples, abx_evals:',abx_evals,', within ABX error:', within_ABX_err)
                    print('After', dtw_samples,'across samples and',within_dtw_samples,
                          'within samples, abx_evals:',abx_evals,', within ABX-kl error:', within_ABX_err_kl)
    
                abx_evals += 1
    
    ABX_err = float(not_correct) / float(not_correct + correct)
    ABX_err_kl = float(kl_not_correct) / float(kl_not_correct + kl_correct)
    within_ABX_err_kl = float(within_kl_not_correct) / float(within_kl_not_correct + within_kl_correct)
    within_ABX_err = float(within_not_correct) / float(within_not_correct + within_correct)
    print('#'*100)
    print('After', dtw_samples, 'samples, final across ABX error:', ABX_err)
    print('After', dtw_samples, 'samples, final across ABX-kl error:', ABX_err_kl)
    print('After', dtw_samples, 'samples, within ABX error:', within_ABX_err)
    print('After', dtw_samples, 'samples, within ABX-kl error:', within_ABX_err_kl)
    
    ABX_results_table_header = ["ABX_err","ABX_DTW_correct","ABX_DTW_incorrect", "ABX_err_kl","ABX_DTW_correct_kl","ABX_DTW_incorrect_kl"]
    ABX_results_table_header_dict = {"ABX_err":0,"ABX_DTW_correct":1,"ABX_DTW_incorrect":2, "ABX_err_kl":3,"ABX_DTW_correct_kl":4,"ABX_DTW_incorrect_kl":5}

    if train_dir[-1] == '/':
        train_dir = train_dir[:-1]
    
    if compute_feat_baseline:
        modelname = train_dir
    else:    
        modelname = train_dir.split("/")[-1]

    if dtw_normalized:
        modelname += '_dtw_normalized'
    else:
        modelname += '_dtw_unnormalized'
        
    if results_file_across != '':
        myutils.save_result_csv(results_file_across, feat_key=modelname, result_key="ABX_err",
                                result=(number_format % ABX_err), results_table_header_dict=ABX_results_table_header_dict,
                                results_table_header=ABX_results_table_header)
        myutils.save_result_csv(results_file_across, feat_key=modelname, result_key="ABX_DTW_correct",
                                result=str(correct), results_table_header_dict=ABX_results_table_header_dict,
                                results_table_header=ABX_results_table_header)
        myutils.save_result_csv(results_file_across, feat_key=modelname, result_key="ABX_DTW_incorrect",
                                result=str(not_correct), results_table_header_dict=ABX_results_table_header_dict,
                                results_table_header=ABX_results_table_header)
        
        myutils.save_result_csv(results_file_across, feat_key=modelname, result_key="ABX_err_kl",
                                result=(number_format % ABX_err_kl), results_table_header_dict=ABX_results_table_header_dict,
                                results_table_header=ABX_results_table_header)
        myutils.save_result_csv(results_file_across, feat_key=modelname, result_key="ABX_DTW_correct_kl",
                                result=str(kl_correct), results_table_header_dict=ABX_results_table_header_dict,
                                results_table_header=ABX_results_table_header)
        myutils.save_result_csv(results_file_across, feat_key=modelname, result_key="ABX_DTW_incorrect_kl",
                                result=str(kl_not_correct), results_table_header_dict=ABX_results_table_header_dict,
                                results_table_header=ABX_results_table_header)
    
    if results_file_within != '':
        myutils.save_result_csv(results_file_within, feat_key=modelname, result_key="ABX_err", result=(number_format % within_ABX_err),
                                results_table_header_dict=ABX_results_table_header_dict, results_table_header=ABX_results_table_header)
        myutils.save_result_csv(results_file_within, feat_key=modelname, result_key="ABX_DTW_correct", result=str(within_correct),
                                results_table_header_dict=ABX_results_table_header_dict, results_table_header=ABX_results_table_header)
        myutils.save_result_csv(results_file_within, feat_key=modelname, result_key="ABX_DTW_incorrect", result=str(within_not_correct),
                                results_table_header_dict=ABX_results_table_header_dict, results_table_header=ABX_results_table_header)
        
        myutils.save_result_csv(results_file_within, feat_key=modelname, result_key="ABX_err_kl", result=(number_format % within_ABX_err_kl),
                                results_table_header_dict=ABX_results_table_header_dict, results_table_header=ABX_results_table_header)
        myutils.save_result_csv(results_file_within, feat_key=modelname, result_key="ABX_DTW_correct_kl", result=str(within_kl_correct),
                                results_table_header_dict=ABX_results_table_header_dict, results_table_header=ABX_results_table_header)
        myutils.save_result_csv(results_file_within, feat_key=modelname, result_key="ABX_DTW_incorrect_kl", result=str(within_kl_not_correct),
                                results_table_header_dict=ABX_results_table_header_dict, results_table_header=ABX_results_table_header)
                        
    # dtw package
    #dist, cost, acc_cost, path = dtw(a, b, dist=lambda x, y: norm(x - y, ord=1))

# cluster the initial representation and give baseline ABX / NMI scores
def cluster_representation(data):
    
    dataX = np.vstack([data[key] for key in data.keys()])
    
    clustered_output = []
    for key in data.keys():
        clustered_output += [data[key]]

def train(curriculum_utt_id_list, utt_id_list):
    
    use_context_features = (FLAGS.filelist_context_features != '')
    
    with tf.device(FLAGS.device):
        with tf.Session(config=tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)) as sess:
            
            with tf.variable_scope("sparse_speech_model"):
                model = SparseSpeech(batch_size=FLAGS.batch_size, do_memory_augment=False, with_context_vector=use_context_features)
            with tf.variable_scope("sparse_speech_model", reuse=True): 
                model_cluster = SparseSpeech(batch_size=FLAGS.batch_size, do_memory_augment=True, with_context_vector=use_context_features)
            with tf.variable_scope("sparse_speech_model", reuse=True):    
                memory_subnet = MemorySubnetwork(batch_size=FLAGS.batch_size)
            
            current_model = model
                     
            model.create_training_graphs()
            model_cluster.create_training_graphs()
            memory_subnet.create_train_op()
            
            idlist_size = len(utt_id_list)
            
            training_start_time = time.time()
            restored = False
            if FLAGS.train_dir != "":
                ckpt = tf.train.get_checkpoint_state(FLAGS.train_dir)
                if ckpt and ckpt.model_checkpoint_path:
                    sess.run(tf.global_variables_initializer())
                    print("Reading model parameters from %s" % ckpt.model_checkpoint_path)
                    current_model.saver.restore(sess, ckpt.model_checkpoint_path)
                    current_model.create_summary_and_train_dir(create_new_train_dir=True, resumed=True)
                    print("model variables:")
                    print(tf.global_variables())
                    restored = True
                else:
                    print("Couldn't load parameters from:" + FLAGS.train_dir)
                    
            
            if not restored:
                print("Created model with fresh parameters.")
                sess.run(tf.global_variables_initializer())
                current_model.create_summary_and_train_dir(create_new_train_dir=True)

            summary_writer = None
            if FLAGS.log_tensorboard:
                summary_writer = tf.summary.FileWriter(current_model.out_dir, sess.graph)

            #write out configuration
            with open(model.out_dir + '/tf_param_train', 'w') as tf_param_train:
                tf_param_train.write(get_FLAGS_params_as_str())

            if FLAGS.grow_sparsity_regularizer_scale != 0.0:
                sparsity_weight = 0.0
                sparsity_weight_growth_per_update = float(FLAGS.grow_sparsity_regularizer_scale) / float(idlist_size/FLAGS.batch_size)
            else:
                sparsity_weight = FLAGS.sparsity_regularizer_scale
                sparsity_weight_growth_per_update = 0.0

            gumbel_temperature = FLAGS.gumbel_temperature_start

            if FLAGS.memory_softmax_type == 'gumbel_softmax':
                print('Using gumbel_softmax. Starting temperature is', gumbel_temperature)

            train_losses = []

            step_time = 0.0
            current_step = 0
            checkpoint_step = 0
            previous_losses = []
            data_batch, len_batch = None, None
            memory_augment = False

            if FLAGS.curriculum_learning:
                epoch = 0
                start_index = 0
                print("Epoch:", epoch)
            
            pdf_writer = None
            
            if FLAGS.pylab_savefigs_pdf and FLAGS.memory_augment_epoch != 0:
                pdf_writer = PdfPages(current_model.out_dir + ('/epoch%i.pdf'%epoch))
            
            data_batch_demo, len_batch_demo, context_features_demo = get_batch_ordered(FLAGS.demo_utterance_num, 1,
                                                                                       curriculum_utt_id_list, idlist_size,
                                                                                       context_feature=use_context_features)
            
            while True:
                
                # recalculate gumbel temperature per update step (Todo: maybe better to do that per epoch?)
                if current_step != 0 and FLAGS.memory_softmax_type == 'gumbel_softmax':
                    if gumbel_temperature > FLAGS.gumbel_temperature_stop:
                        gumbel_temperature *= FLAGS.gumbel_temperature_scale
                               
                current_step += 1

#                if current_step % FLAGS.steps_per_summary == 0 and summary_writer is not None:
#                    #input_window_1, input_window_2, labels = model.get_batch_k_samples(filelist=filelist, window_length=FLAGS.window_length, window_neg_length=FLAGS.window_neg_length, k=FLAGS.negative_samples)
#                    
#                    feed_dict_summary = {model.input_window_1:input_window_1, model.input_window_2:input_window_2, model.labels: labels}
#                    
#                    if FLAGS.dynamic_windows:
#                        feed_dict_summary[model.input_sequence1_length] = window_sequence_lengths
#                        feed_dict_summary[model.input_sequence2_length] = window_neg_sequence_lengths
#                    
#                    summary_str = sess.run(model.train_summary_op, feed_dict=feed_dict_summary)
#                    
#                    summary_writer.add_summary(summary_str, current_step)

                # Get a batch and make a step.
                start_time = time.time()
                
                if epoch == FLAGS.memory_augment_epoch and not memory_augment:
                     memory_augment = True
                     # we reset previous best losses so that the saver will safe the new model
                     previous_losses = []
                     enc_outs = []
                     
                     if FLAGS.pretrain_kmeans:
                         for i in xrange(FLAGS.memory_init_samples):
                             random_start = int(math.floor(np.random.random_sample() * float(idlist_size)))
                             
                             data_batch, len_batch, context_features = get_batch_ordered(random_start, FLAGS.batch_size, 
                                                                                         curriculum_utt_id_list, idlist_size,
                                                                                         context_feature=use_context_features)
    
                             if use_context_features:
                                 enc_out = sess.run(model.enc_out, feed_dict={model.input_sequence: data_batch, model.input_sequence_length: len_batch,
                                                                              model.context_vector: context_features,
                                                                              model.gumbel_temperature: FLAGS.gumbel_generate_temperature})
                             else:
                                 enc_out = sess.run(model.enc_out, feed_dict={model.input_sequence: data_batch, model.input_sequence_length: len_batch,
                                                                              model.gumbel_temperature: FLAGS.gumbel_generate_temperature})
                             enc_outs.append(np.copy(np.vstack(enc_out)[::10]))
                         enc_out_stacked = np.vstack(enc_outs)
                         
                         print("Starting Kmeans++ on enc_out_stacked with " + str(enc_out_stacked.shape[0])+" vectors from " +
                               str(FLAGS.memory_init_samples*FLAGS.batch_size) + " sequences.")
                         
                         if FLAGS.cluster_algo == "tensorflow_kmeans":
                             num_iterations = 150
                             #SQUARED_EUCLIDEAN_DISTANCE
                             #COSINE_DISTANCE
                             input_fn=lambda: tf.data.Dataset.from_tensors(tf.convert_to_tensor(enc_out_stacked, dtype=get_dtype(FLAGS.dtype)))

                             kmeans = kmeans_lib.KMeansClustering(num_clusters=FLAGS.num_mem_entries, 
                                                                  distance_metric=kmeans_lib.KMeansClustering.SQUARED_EUCLIDEAN_DISTANCE,
                                                                  use_mini_batch=False)
        
                             kmeans.train(input_fn=input_fn,steps=num_iterations)
                             
                             cluster_centers = kmeans.cluster_centers()
                             
                             predictions = np.asarray([elem['cluster_index'] for elem in kmeans.predict(input_fn)])
                             
                             print(predictions)
                             print(predictions.shape)
                             
                             print('Cluster center type:', type(cluster_centers))
                         elif FLAGS.cluster_algo == "hdbscan":
                         
                             clusterer = hdbscan.HDBSCAN(min_cluster_size=100).fit(enc_out_stacked)
                             n = max(clusterer.labels_)
                             
                             predictions = np.asarray(clusterer.labels_)
                             
                             print("Hdbscan found", n, "clusters.")
                            
                         elif FLAGS.cluster_algo == "sklearn_kmeans":
                             #use sklearn
                             kmeans = KMeans(n_clusters=FLAGS.num_mem_entries, init='k-means++', n_init = FLAGS.n_jobs_cluster,
                                                                             n_jobs = FLAGS.n_jobs_cluster).fit(enc_out_stacked)
                             cluster_centers = np.asarray(kmeans.cluster_centers_)
                             
                             predictions = np.asarray(kmeans.labels_)
                         elif FLAGS.cluster_algo == "DPGMM":
                             max_iter = 200
                             dpgmm = sklearn.mixture.BayesianGaussianMixture(n_components=FLAGS.num_mem_entries, covariance_type='full',
                                                                            tol=0.001, reg_covar=1e-06, max_iter=max_iter,
                                                                            n_init=1, init_params='kmeans', weight_concentration_prior_type='dirichlet_process',
                                                                            weight_concentration_prior=None, mean_precision_prior=None, mean_prior=None,
                                                                            degrees_of_freedom_prior=None, covariance_prior=None, random_state=42,
                                                                            warm_start=False, verbose=1000, verbose_interval=1)
                             
                             dpgmm.fit(enc_out_stacked)
                             
                             cluster_centers = dpgmm.means_
                             predictions = np.asarray(dpgmm.predict(enc_out_stacked))
                         elif FLAGS.cluster_algo == "none":
                             print("Not clustering.")
                             
                         if FLAGS.pylab_show_feats:
                             plt.imshow(cluster_centers.T, interpolation=None, aspect='auto', origin='lower')
                             plt.show()
    
                         #sess.run(tf.assign(model_cluster.key_memory, cluster_centers*-1.0)) # / np.expand_dims(np.linalg.norm(cluster_centers, ord=2, axis=1),1)))
                         #sess.run(tf.assign(model_cluster.key_memory, cluster_centers))
                         sess.run(tf.assign(model_cluster.value_memory, cluster_centers))
                         
                         subnetwork_batchsize=1024
                         for i in xrange(FLAGS.memory_subnetwork_trainingsteps):
                             cost, _ = sess.run([memory_subnet.cost, memory_subnet.train_op], feed_dict={memory_subnet.enc_out_flattened_in: enc_out_stacked,
                                                memory_subnet.labels_in: predictions})
    
                             if i%100 == 0:
                                 print('Memory subnetwork, step:', i ,'cost:', cost)
                         
                         key_memory_W = sess.run(memory_subnet.key_memory_W)   
                         key_memory_b = sess.run(memory_subnet.key_memory_b)   
                            
                         #if FLAGS.pylab_show_feats:
                         #    plt.imshow(key_memory.T, interpolation=None, aspect='auto', origin='lower')
                         #    plt.show()
                         
                         #sess.run(tf.assign(model_cluster.key_memory,key_memory))
                         
                         sess.run(tf.assign(model_cluster.key_memory_W,key_memory_W))
                         sess.run(tf.assign(model_cluster.key_memory_b,key_memory_b))
                     
                     # switch to memory augmented model
                     current_model = model_cluster             
                     current_model.create_summary_and_train_dir(create_new_train_dir=True, resumed=True)
                     
                     if FLAGS.pylab_savefigs_pdf:
                        if pdf_writer is not None:
                             pdf_writer.close()
                     
                        pdf_writer = PdfPages(current_model.out_dir + ('/epoch%i.pdf'%epoch))

                     
                     if FLAGS.log_tensorboard:
                         summary_writer = tf.summary.FileWriter(current_model.out_dir, sess.graph)
                     #ARI_cae_baseline = metrics.adjusted_rand_score(kmeans.labels_, ytrain)
                     #print('ARI score (kmeans cae baseline):', number_format % ARI_cae_baseline)
                     #vmeasure_cae_baseline = metrics.v_measure_score(kmeans.labels_, ytrain)
                     #print('NMI / V-measure (kmeans cae baseline):', number_format % vmeasure_cae_baseline)
                
                if FLAGS.curriculum_learning:
                    # get a data batch from the sorted training set (by length)
                    # this makes sure that we learn from the shorter and easier examples first and that we group batches of similar length (for efficiency)
                    data_batch, len_batch, context_features = get_batch_ordered(start_index, FLAGS.batch_size,
                                                                                curriculum_utt_id_list, idlist_size,
                                                                                context_feature=use_context_features)
                    last_batch_start_index = start_index
                    start_index += FLAGS.batch_size
                    if start_index > idlist_size:
                        start_index = 0
                        epoch += 1
                        print("Epoch:", epoch,"1 epoch = ",idlist_size,"utterances")
                        if FLAGS.pylab_savefigs_pdf:
                            if pdf_writer is not None:
                                pdf_writer.close()   
                            pdf_writer = PdfPages(current_model.out_dir + ('/epoch%i.pdf'%epoch))
                            
                            # TODO write an example query of an utterance out (for a single example)
                            #pdf_writer_example = PdfPages(current_model.outdir + ('epoch%i_ex.pdf'%epoch))
                            #print('Generating an example file for the pdf writer.')
                            #data_batch_inspect, len_batch_inspect = get_batch_ordered(420, 1, curriculum_utt_id_list, idlist_size)
                            #out, train_loss, sparsity_loss, diversity_loss, dwelling_loss , dec, enc, enc_out, query_out, bridge = current_model.step(sess, input_sequence=data_batch, input_sequence_length=len_batch, output_sequence=data_batch, context_vector=None)

                else:
                    data_batch, len_batch, context_features = get_batch(FLAGS.batch_size, utt_id_list, idlist_size, context_feature=use_context_features)
                    
                    # While this is not a true epoch, when we randomly generate batches, one epoch = as many updates as there are examples in the dataset
                    if current_step*FLAGS.batch_size > (epoch+1)*idlist_size:
                        epoch += 1
                
                if epoch >= FLAGS.train_epochs:
                    if FLAGS.pylab_savefigs_pdf:
                        if pdf_writer is not None:
                            pdf_writer.close()  
                    break
                #debug_key_memory_grads = sess.run(model_cluster.debug_key_memory_grads,
                #                                  feed_dict = {model_cluster.input_sequence: data_batch, model_cluster.input_sequence_length: len_batch,
                #                                               model_cluster.output_sequence: data_batch})
                
                #print('grad:', debug_key_memory_grads)
                
                # output sequence had a copy list()
                out, train_loss, sparsity_loss, diversity_loss, dwelling_loss , dec, enc, enc_out, query_out, bridge = current_model.step(sess,
                                    input_sequence=data_batch, input_sequence_length=len_batch, output_sequence=data_batch, context_vector=context_features,
                                    sparsity_weight=sparsity_weight, gumbel_temperature=gumbel_temperature, gumbel_noise_weight=1.0)
                
                train_losses.append(train_loss)

                sparsity_weight += sparsity_weight_growth_per_update
                
                step_time += (time.time() - start_time) / FLAGS.steps_per_checkpoint
                
                # Once in a while, we save checkpoint, print statistics, and run evals.
                if current_step % FLAGS.steps_per_checkpoint == 2:
                    checkpoint_step += 1
                    mean_train_loss = np.mean(train_losses)

                    if FLAGS.pylab_show_feats and memory_augment:
                        #key_memory = sess.run(model_cluster.key_memory)
                        value_memory = sess.run(model_cluster.value_memory)

                        print("value memory:")
                        
                        plt.imshow(value_memory.T, interpolation=None, aspect='auto', origin='lower')
                        plt.show()
                        #key_memory_old = key_memory
                        #value_memory_old = value_memory
                    
                    
                    if FLAGS.pylab_show_feats or FLAGS.pylab_savefigs_pdf:
                        f, axarr = plt.subplots(6 if memory_augment else 4, sharex=True, figsize=(28, 14))
                        if FLAGS.curriculum_learning:
                            print('Showing start index', last_batch_start_index , "with utt_id",
                                  curriculum_utt_id_list[last_batch_start_index])
                        
                        print("Sequence / out / dec / enc / query_out / bridge:")
                        for i,ax in enumerate(axarr):
                            if i == 0:
                                ax.imshow(data_batch[0].T, interpolation=None, aspect='auto', origin='lower')
                            if i == 1:
                                ax.imshow(out[0].T, interpolation=None, aspect='auto', origin='lower')
                            if i == 2:
                                ax.imshow(dec[0].T, interpolation=None, aspect='auto', origin='lower')
                            if i == 3:
                                ax.imshow(enc[0].T, interpolation=None, aspect='auto', origin='lower')
                            if i == 4 and memory_augment:
                                print("query_out.shape():", query_out.shape)
                                print("query_out[0].T.shape():", query_out[0].T.shape)
                                ax.imshow(query_out[0].T, interpolation=None, aspect='auto', origin='lower')
                            if i == 5 and memory_augment:
                                ax.imshow(bridge[0].T, interpolation=None, aspect='auto', origin='lower')
                        if FLAGS.pylab_savefigs_pdf:
                            pdf_writer.savefig()
                        if FLAGS.pylab_show_feats:
                            plt.show()
                        else:
                            if FLAGS.pylab_savefigs_pdf:
                                plt.close()
                        
                        feed_dict_demo = {current_model.input_sequence:data_batch_demo, 
                                          current_model.input_sequence_length:len_batch_demo,
                                          current_model.output_sequence:data_batch_demo,
                                          current_model.sparsity_weight:sparsity_weight,
                                          current_model.gumbel_temperature:gumbel_temperature,
                                          current_model.gumbel_noise_weight:0.0}
                        
                        if use_context_features:
                            feed_dict_demo[current_model.context_vector] = context_features_demo
                        
                        out_demo, train_loss_demo, sparsity_loss_demo, diversity_loss_demo, dwelling_loss_demo, \
                        dec_demo, enc_demo, enc_out_demo, query_out_demo, bridge_demo = sess.run([current_model.out, \
                                            current_model.cost, current_model.sparsity_loss, current_model.diversity_loss, \
                                            current_model.dwelling_loss, current_model.dec, current_model.enc, current_model.enc_out, \
                                            current_model.query_out, current_model.bridge], feed_dict=feed_dict_demo)

                        f, axarr = plt.subplots(6 if memory_augment else 4, sharex=True, figsize=(28, 14))
                        if FLAGS.curriculum_learning:
                            print('Showing demo utterance', FLAGS.demo_utterance_num , "with utt_id",
                                  curriculum_utt_id_list[FLAGS.demo_utterance_num])
                            
                        for i,ax in enumerate(axarr):
                            if i==0:
                                ax.imshow(data_batch_demo[0].T, interpolation=None, aspect='auto', origin='lower')
                            if i==1:
                                ax.imshow(out_demo[0].T, interpolation=None, aspect='auto', origin='lower')
                            if i==2:
                                ax.imshow(dec_demo[0].T, interpolation=None, aspect='auto', origin='lower')
                            if i==3:
                                ax.imshow(enc_demo[0].T, interpolation=None, aspect='auto', origin='lower')
                            if i==4 and memory_augment:
                                print("query_out.shape():", query_out_demo.shape)
                                print("query_out[0].T.shape():", query_out_demo[0].T.shape)
                                ax.imshow(query_out_demo[0].T, interpolation=None, aspect='auto', origin='lower')
                            if i==5 and memory_augment:
                                ax.imshow(bridge_demo[0].T, interpolation=None, aspect='auto', origin='lower')
                                
                        if FLAGS.pylab_savefigs_pdf:
                            pdf_writer.savefig()
                        if FLAGS.pylab_show_feats:
                            plt.show()
                        else:
                            if FLAGS.pylab_savefigs_pdf:
                                plt.close()
                    
                    print('At step %i step-time %.4f loss %.4f sparsity loss %.4f diversity loss %.4f dwelling loss %.4f' 
                          % (current_step, step_time, mean_train_loss, sparsity_loss, diversity_loss, dwelling_loss))
                    if FLAGS.curriculum_learning:
                        print('At epoch:', epoch,"1 epoch = ",idlist_size,"utterances")

                    print('Model saving path is:', current_model.out_dir)
                    print('Training started', (time.time() - training_start_time) / 3600.0,'hours ago.')
                    print('FLAGS params in short:',get_model_flags_param_short())
                    print('sparsity_weight is:', sparsity_weight, ' sparsity_weight_growth_per_update is:' , sparsity_weight_growth_per_update)

                    if FLAGS.memory_softmax_type == 'gumbel_softmax':
                        print('Using gumbel_softmax. Current temperature is', gumbel_temperature)
                        print('Gumbel start:', FLAGS.gumbel_temperature_start, 'gumbel stop:', FLAGS.gumbel_temperature_stop)
                    else:
                        print('Using:',FLAGS.memory_softmax_type,'as memory softmax.')

                    train_losses = []
                    step_time = 0
                    if checkpoint_step % FLAGS.checkpoints_per_save == 1:
                        min_loss = 1e10
                        if len(previous_losses) > 0:
                            min_loss = min(previous_losses)
                        if mean_train_loss < min_loss:
                            print(('Mean train loss: %.6f' % mean_train_loss) + (' is smaller than previous best loss: %.6f' % min_loss) )
                            print('Saving the best model so far to ', current_model.out_dir, '...')
                            current_model.saver.save(sess, current_model.out_dir, global_step=current_model.global_step)
                            previous_losses.append(mean_train_loss)
    
    # return model output path after training finish (so that we can evaluate it etc.)                     
    return model_cluster.out_dir if memory_augment else model.out_dir


def filter_phone(phone):
    phone = phone.split('_')[0]
    if phone[-1].isnumeric():
        phone = phone[:-1]
    return phone


if __name__ == '__main__':
    FLAGS(sys.argv)
    # pre tf 1.5:
    #FLAGS._parse_flags()
    print("\nParameters:")
    print(get_FLAGS_params_as_str())
    
    alignment_data = None
    if FLAGS.ali_ctm != '':
        print("Reading alignment information from: " + FLAGS.ali_ctm)
        alignment_data = myutils.readAlignmentFile(FLAGS.ali_ctm) 
        print("Done! Alignment file has",len(alignment_data.keys()),"utterances!")
        
        with open(FLAGS.phones) as phones_in:
            for line in phones_in:
                split = line.split()
                phone = split[0]
                number = int(split[1])
                phones_data[number] = phone   

        phones_reduced = sorted(list(set([filter_phone(phone) for phone in phones_data.values() 
                                    if not phone.startswith('#') and not phone.startswith('<')])))
        
        print(phones_reduced)
        
        phones_reduced_trans_data_phn = dict([(number,filter_phone(phone)) 
                                        for number, phone in phones_data.items() if not phone.startswith('#') and not phone.startswith('<')])
        
        phones_reduced_trans_data = dict([(int(number),phones_reduced.index(filter_phone(phone))) 
                                        for number, phone in phones_data.items() if not phone.startswith('#') and not phone.startswith('<')])
                                                                              
        print(phones_reduced_trans_data)
    else:
        print("No boundary information available (alignments information via --ali_ctm), not doing evaluations.")
    
    if FLAGS.ali_word_ctm != '':
        print("Reading word alignment information from: " + FLAGS.ali_word_ctm)
        word_alignment_data = myutils.readAlignmentFile(FLAGS.ali_word_ctm)
        print("Done!")
    
    if FLAGS.filelist.endswith('.scp'):
        print('Now reading features from Kaldi scp file:', FLAGS.filelist)
        features, utt_id_list = kaldi_io.readScp(FLAGS.filelist, limit = FLAGS.debug_limit if FLAGS.debug else np.inf) #, memmap_dir=FLAGS.memmap_dir, memmap_dtype=FLAGS.memmap_dtype)
        orig_feat_len = len(utt_id_list)
        print('Done, read:',orig_feat_len,'features!')
    elif FLAGS.filelist.endswith('.ark'):
        print('Now reading features from Kaldi ark file:', FLAGS.filelist)
        features, utt_id_list = kaldi_io.readArk(FLAGS.filelist, limit = FLAGS.debug_limit if FLAGS.debug else np.inf) #, memmap_dir=FLAGS.memmap_dir, memmap_dtype=FLAGS.memmap_dtype)
        orig_feat_len = len(utt_id_list)
        print('Done, read:',orig_feat_len,'features!')

    if FLAGS.utt_len_file != '':
        with open(FLAGS.utt_len_file) as utt_lens:
            lens = []
            for line in utt_lens:
                line.strip()
                lens += [float(line)]
        
        print("Fitting utterance length sampler...")
        
        utt_len_sampler = mixture.GaussianMixture(n_components=2, verbose=100, max_iter=10000)
        utt_len_sampler.fit(np.asarray(lens,dtype=np.float32).reshape(-1,1))
        
        # sample example:
        #utt_len_sampler.sample(n_samples=1000000)[0].reshape(-1)
        
        print("Done!")

    #min_required_sampling_length = 10
    ##trim training data to minimum required sampling length
    #training_data = {key: value for (key, value) in zip(utt_id_list, features) if value.shape[0] > min_required_sampling_length}
    
    if FLAGS.filelist_context_features.endswith('.scp'):
        print('Now reading features from Kaldi scp file:', FLAGS.filelist_context_features)
        context_features, context_utt_id_list = kaldi_io.readScp(FLAGS.filelist_context_features, limit = FLAGS.debug_limit if FLAGS.debug else np.inf) 
        context_orig_feat_len = len(context_utt_id_list)
        print('Done, read:',context_orig_feat_len,'features!')
    elif FLAGS.filelist_context_features.endswith('.ark'):
        print('Now reading features from Kaldi ark file:', FLAGS.filelist_context_features)
        context_features, context_utt_id_list = kaldi_io.readArk(FLAGS.filelist_context_features, limit = FLAGS.debug_limit if FLAGS.debug else np.inf)
        context_orig_feat_len = len(context_utt_id_list)
        print('Done, read:',context_orig_feat_len,'features!')
        #, memmap_dir=FLAGS.memmap_dir, memmap_dtype=FLAGS.memmap_dtype)

    if FLAGS.smooth_feats:
        print("Smoothing input features...")
        for i in xrange(len(features)):
            features[i] = convolve2d(features[i], np.asarray([[.05,.05,.05],[.5,.6,.05],[.05,.05,.05]]), boundary='symm', mode='same')
        print('Done!')
    
    if alignment_data is None:
        training_data = {key: value for (key, value) in zip(utt_id_list, features)}
    else:
        training_data = {key: value for (key, value) in zip(utt_id_list, features) if key in alignment_data}
        utt_id_list = [elem for elem in utt_id_list if elem in alignment_data]
    
    if FLAGS.spk2utt != "":
        spk2utt_data_orig = myutils.loadSpk2Utt(FLAGS.spk2utt, ignore_dash_spk_id=True)
        # filter spk2utt and make sure we have all the training data
        print('Original spk2utt speakers:', len(spk2utt_data_orig))
        for spk in spk2utt_data_orig:
            spk_ids = [myid for myid in spk2utt_data_orig[spk] if myid in training_data]
            if len(spk_ids) > 0:
                spk2utt_data[spk] = spk_ids
        print('Final spk2utt speakers after filtering:', len( spk2utt_data))        
    
    if FLAGS.filelist_context_features is not None and FLAGS.filelist_context_features != "":
        print('Genearting mean vectors for:' + FLAGS.filelist_context_features)
        for feature, elem in zip(context_features, context_utt_id_list):
            context_training_data[elem] = np.mean(feature, axis=0)
#            print(context_training_data[elem])
        del context_features
            
    #check if all context features are there    
    if FLAGS.filelist_context_features is not None and FLAGS.filelist_context_features != "":    
        for key in training_data:
            if key not in context_training_data:
                print('Problem with context feature file '+FLAGS.filelist_context_features+' detected, does not contain id:', key)
                sys.exit()     
    
    curriculum_utt_id_list_with_len = [(utt_id, training_data[utt_id].shape[0]) for utt_id in utt_id_list if training_data[utt_id].shape[0] < FLAGS.max_seq_len]
    curriculum_utt_id_list = [elem[0] for elem in sorted(curriculum_utt_id_list_with_len, key=lambda x: x[1])]
            
    utt_id_list = [elem[0] for elem in curriculum_utt_id_list_with_len]
    # done 10.0 and 1.0
    print("Final filtered utt_id_list len:", len(utt_id_list))


    if FLAGS.evaluate:
        if FLAGS.baseline_feat_eval:
            evaluate_ABX(FLAGS.modelname + "_baseline_feat", curriculum_utt_id_list, len(utt_id_list), results_file_within="eval_results_file_word_ABX_within.csv",
                         results_file_across="results_file_word_ABX_across.csv", phn_trigram=False, compute_feat_baseline=True, dtw_normalized=FLAGS.dtw_normalized)
            evaluate_ABX(FLAGS.modelname + "_baseline_feat", curriculum_utt_id_list, len(utt_id_list), results_file_within="eval_results_file_triphn_ABX_within.csv",
                         results_file_across="results_file_triphn_ABX_across.csv", phn_trigram=True, compute_feat_baseline=True, dtw_normalized=FLAGS.dtw_normalized)
        
        elif FLAGS.DPGMM_cluster_baseline:
            model = baselineDPGMM()
            
            if FLAGS.DPGMM_baseline_model is None or FLAGS.DPGMM_baseline_model == '':
                print("Training model")
                model.train(curriculum_utt_id_list[:8000])
                print("Done!")
                FLAGS.DPGMM_cluster_baseline = model.save(FLAGS.modelname)
                print('Saved model to:', FLAGS.DPGMM_cluster_baseline )
            else:
                print('Loading model from:' + FLAGS.DPGMM_baseline_model)
                model.load(FLAGS.DPGMM_baseline_model)
                
            prediction = model.predict(training_data[utt_id_list[420]])
            
            print("prediction:", prediction)
            
            prediction = model.predict_proba(training_data[utt_id_list[420]])
            
            print("prediction:", prediction)
            
            f, axarr = plt.subplots(2, sharex=True, figsize=(28, 14))

            for i,ax in enumerate(axarr):
                if i==0:
                    ax.imshow(prediction.T, interpolation=None, aspect='auto', origin='lower')
                elif i==1:
                    ax.imshow(training_data[utt_id_list[420]].T, interpolation=None, aspect='auto', origin='lower')

            if FLAGS.pylab_show_feats:
                plt.show()
            else:
                if FLAGS.pylab_savefigs_pdf:
                    plt.close()
            if FLAGS.gen_zerospeech_dir is not None and FLAGS.gen_zerospeech_dir != '':
                time_start = 125
                time_inc = 100
                
                for key in training_data:
                    current_time = time_start
                    with open(FLAGS.gen_zerospeech_dir + '/' + key.replace('.wav','.fea')) as feat_out:
                        print('Writing' + FLAGS.gen_zerospeech_dir)
                        feats = model.predict_proba(training_data[key])
                        for feat in feats:                      
                            time_out = current_time / 10000.0
                            feat_out.write(str(time_out) + ' ' + ' '.join(list(feat)) + '\n')
                            current_time += time_inc
            else:
                tf.reset_default_graph()
                evaluate_ABX(FLAGS.modelname + "_baseline_feat_DPGMM_" + FLAGS.DPGMM_baseline_model, curriculum_utt_id_list,
                             len(utt_id_list), results_file_within="results_file_triphn_ABX_within.csv",
                             results_file_across="results_file_triphn_ABX_across.csv", phn_trigram=True,
                             compute_feat_baseline=True, dtw_normalized=FLAGS.dtw_normalized, DPGMM_baseline_model=model)
                tf.reset_default_graph()
                evaluate_ABX(FLAGS.modelname + "_baseline_feat_DPGMM_" + FLAGS.DPGMM_baseline_model, curriculum_utt_id_list,
                             len(utt_id_list), results_file_within="results_file_word_ABX_within.csv",
                             results_file_across="results_file_word_ABX_across.csv", phn_trigram=False,
                             compute_feat_baseline=True, dtw_normalized=False, DPGMM_baseline_model=model)
            #demo_outterance_id = '4535-279852-0038'
        else:
            do_gen_kaldi_feat = (FLAGS.gen_kaldi_file is not None and FLAGS.gen_kaldi_file != '')
            if (FLAGS.gen_zerospeech_dir is not None and FLAGS.gen_zerospeech_dir != '') or do_gen_kaldi_feat:
                write_unsup_challenge_feat(FLAGS.train_dir, curriculum_utt_id_list, only_pretrain=FLAGS.only_eval_pretrain,
                                           bridge_without_concat=FLAGS.bridge_without_concat, kaldi_feat=do_gen_kaldi_feat,
                                           kaldi_output_text_file=FLAGS.gen_kaldi_file)
            else:
                for eval_model in FLAGS.evaluate_models.split(':'):
                    tf.reset_default_graph()
                    evaluate_ABX(FLAGS.train_dir, curriculum_utt_id_list, len(utt_id_list), results_file_within="results_file_triphn_ABX_within.csv",
                                 results_file_across="results_file_triphn_ABX_across.csv", phn_trigram=True, dtw_normalized=FLAGS.dtw_normalized)
                    tf.reset_default_graph()
                    evaluate_ABX(FLAGS.train_dir, curriculum_utt_id_list, len(utt_id_list), results_file_within="results_file_word_ABX_within.csv",
                                 results_file_across="results_file_word_ABX_across.csv", phn_trigram=False, dtw_normalized=False)
    else:
        if FLAGS.scan_sparsity_values:
            #for n in [16,10]: #[16,20,42,80,64]: #,60,80,100,120]: #,80,160]:
            for sparsity in [2.0,0.02,0.05,0.1,2.5,3.0,5.0,7.5,10.0]: #0.0,0.5,2.5,5.0,10.0,30.0]: #[10.0, 50.0, 80.0, 100.0, 150.0, 200.0, 300.0]: #, 50.0, 100.0, 150.0, 200.0, 300.0]:
                #print('Setting cluster n to', n)
                #FLAGS.num_mem_entries = n
                print('Setting diversity to:', sparsity)
                FLAGS.sparsity_regularizer_scale=sparsity
                #FLAGS.diversity_regularizer_scale=sparsity

                current_model_train_dir = train(curriculum_utt_id_list, utt_id_list)
                tf.reset_default_graph()
                #current_model_train_dir = ""
                print('Training has finished, now calling evaluate minimal triphone ABX')
                tf.reset_default_graph()
                evaluate_ABX(current_model_train_dir, curriculum_utt_id_list, len(utt_id_list), results_file_within="results_file_triphn_ABX_within.csv",
                             results_file_across="results_file_triphn_ABX_across.csv", phn_trigram=True, dtw_normalized=FLAGS.dtw_normalized)
            
                print('Training has finished, now calling evaluate word ABX')
                tf.reset_default_graph()
                evaluate_ABX(current_model_train_dir, curriculum_utt_id_list, len(utt_id_list), results_file_within="results_file_word_ABX_within.csv",
                             results_file_across="results_file_word_ABX_across.csv", phn_trigram=False, dtw_normalized=False)
                
                print('Training has finished, now calling evaluate NMI')
                try:
                    evaluate_NMI(current_model_train_dir, curriculum_utt_id_list, len(utt_id_list), results_file="results_file_NMI.csv") 
                except:
                    print('Warning: error in evaluate_NMI')
                tf.reset_default_graph()
    
        else:
            current_model_train_dir = train(curriculum_utt_id_list, utt_id_list)
            if FLAGS.evaluate_after_train:
                tf.reset_default_graph()
                #current_model_train_dir = ""
                print('Training has finished, now calling evaluate minimal triphone ABX')
                tf.reset_default_graph()
                evaluate_ABX(current_model_train_dir, curriculum_utt_id_list, len(utt_id_list), results_file_within="results_file_triphn_ABX_within.csv",
                             results_file_across="results_file_triphn_ABX_across.csv", phn_trigram=True, dtw_normalized=FLAGS.dtw_normalized)
            
                print('Training has finished, now calling evaluate word ABX')
                tf.reset_default_graph()
                evaluate_ABX(current_model_train_dir, curriculum_utt_id_list, len(utt_id_list), results_file_within="results_file_word_ABX_within.csv",
                             results_file_across="results_file_word_ABX_across.csv", phn_trigram=False, dtw_normalized=False)
