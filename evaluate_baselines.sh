#!/bin/bash
#python3 sparse_seq_ae.py --evaluate --baseline_feat_eval --modelname plp_cmvn --filelist feats/librispeech_360/plp_360_feats_cmvn.ark --dtw_normalized False --device /cpu:1
python3 sparse_seq_ae.py --evaluate --baseline_feat_eval --modelname plp --filelist feats/librispeech_360/plp_360_feats.ark --nodtw_normalized --device /cpu:1
